//
//  CaptureResult.h
//  mcid_sdk
//
//  Created by Enrique Álvarez on 22/10/2018.
//  Copyright © 2018 Gemalto. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CaptureResult : NSObject

@property (nonatomic, strong, readonly) NSData *side1;
@property (nonatomic, strong, readonly) NSData *side2;
@property (nonatomic, strong, readonly) NSDictionary *metadata;
@property (nonatomic, strong, readonly) NSDictionary *mrz;

- (instancetype)initWithSide1:(NSData *)side1
                        side2:(NSData *)side2
                     metadata:(NSDictionary *)metadata
                          mrz:(NSDictionary *)mrz;

@end
