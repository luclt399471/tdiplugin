#import <Foundation/Foundation.h>

@class MainFailureCompanion, MainStatus, MainKotlinEnum, MainTdiHttpFailure, MainKotlinException, MainKotlinThrowable, MainKotlinArray, MainAbstractCaptorProviderImpl, MainAbstractCaptorProviderImplCompanion, MainAbstractSessionImpl, MainKotlinUnit, MainScenarioInfo, MainSessionStateManager, MainAbstractSessionImplCompanion, MainInputImpl, MainHttpApiFactoryImpl, MainHttpApiFactory, MainStatePatchRequest, MainBuildUtils, MainPullTaskStateHandlerImpl, MainTdiConstants, MainIOSSessionImpl, MainIOSSessionStateManager, MainKotlinPair, MainResultAdapter, MainSessionStateManagerCompanion, MainTaskAdapter, MainTaskImpl, MainInitRequester, MainInitRequest, MainInitRequesterCompanion, MainTdiClient, MainKtor_client_coreHttpClient, MainInitRequestCompanion, MainInitRequest$serializer, MainStatePatchRequestCompanion, MainStatePatchRequest$serializer, MainIos, MainKtor_client_coreHttpClientEngineConfig, MainIosClientEngineConfig, MainIosClientEngine, MainKotlinx_coroutines_coreCoroutineDispatcher, MainKtor_client_coreHttpResponseConfig, NSMutableURLRequest, MainIosHttpRequestException, NSError, MainLogger, MainLoggerCompanion, MainState, MainResult, MainStep, MainKtor_client_coreHttpClientConfig, MainKtor_client_coreHttpReceivePipeline, MainKtor_client_coreHttpRequestPipeline, MainKtor_client_coreHttpResponsePipeline, MainKtor_client_coreHttpSendPipeline, MainKotlinx_serialization_runtime_nativeEnumDescriptor, MainKotlinx_serialization_runtime_nativeSerialKind, MainKotlinNothing, MainKotlinx_serialization_runtime_nativeUpdateMode, MainKotlinAbstractCoroutineContextElement, MainKotlinx_ioCharset, MainKtor_utilsAttributeKey, MainKtor_utilsPipeline, MainKtor_utilsPipelinePhase, MainKotlinx_serialization_runtime_nativeSerialClassDescImpl, MainKotlinx_ioCharsetDecoder, MainKotlinx_ioCharsetEncoder;

@protocol MainCaptor, MainCaptorCallback, MainInput, MainFailure, MainCaptorProvider, MainFcmMessagingHandler, MainResult, MainSession, MainTask, MainKotlinComparable, MainCaptorFactory, MainCaptorInvoker, MainPullTaskStateHandler, MainTdiRequester, MainPushTokenProvider, MainKotlinx_serialization_runtime_nativeKSerializer, MainKotlinx_serialization_runtime_nativeGeneratedSerializer, MainKotlinx_serialization_runtime_nativeSerializationStrategy, MainKotlinx_serialization_runtime_nativeEncoder, MainKotlinx_serialization_runtime_nativeSerialDescriptor, MainKotlinx_serialization_runtime_nativeDeserializationStrategy, MainKotlinx_serialization_runtime_nativeDecoder, MainKtor_client_coreHttpClientEngineFactory, MainKtor_client_coreHttpClientEngine, MainKotlinx_coroutines_coreCoroutineScope, MainKotlinCoroutineContext, MainKotlinx_ioCloseable, MainKotlinIterator, MainKtor_utilsAttributes, MainKotlinx_serialization_runtime_nativeCompositeEncoder, MainKotlinx_serialization_runtime_nativeSerialModule, MainKotlinAnnotation, MainKotlinx_serialization_runtime_nativeCompositeDecoder, MainKotlinCoroutineContextElement, MainKotlinCoroutineContextKey, MainKotlinContinuationInterceptor, MainKotlinContinuation, MainKotlinx_coroutines_coreRunnable, MainKtor_client_coreHttpClientFeature, MainKotlinSuspendFunction2, MainKotlinx_serialization_runtime_nativeSerialModuleCollector, MainKotlinKClass, MainKotlinSuspendFunction, MainKotlinKDeclarationContainer, MainKotlinKAnnotatedElement, MainKotlinKClassifier;

NS_ASSUME_NONNULL_BEGIN

@interface KotlinBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface KotlinBase (KotlinBaseCopying) <NSCopying>
@end;

__attribute__((objc_runtime_name("KotlinMutableSet")))
__attribute__((swift_name("KotlinMutableSet")))
@interface MainMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((objc_runtime_name("KotlinMutableDictionary")))
__attribute__((swift_name("KotlinMutableDictionary")))
@interface MainMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((objc_runtime_name("KotlinNumber")))
__attribute__((swift_name("KotlinNumber")))
@interface MainNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((objc_runtime_name("KotlinByte")))
__attribute__((swift_name("KotlinByte")))
@interface MainByte : MainNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((objc_runtime_name("KotlinUByte")))
__attribute__((swift_name("KotlinUByte")))
@interface MainUByte : MainNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((objc_runtime_name("KotlinShort")))
__attribute__((swift_name("KotlinShort")))
@interface MainShort : MainNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((objc_runtime_name("KotlinUShort")))
__attribute__((swift_name("KotlinUShort")))
@interface MainUShort : MainNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((objc_runtime_name("KotlinInt")))
__attribute__((swift_name("KotlinInt")))
@interface MainInt : MainNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((objc_runtime_name("KotlinUInt")))
__attribute__((swift_name("KotlinUInt")))
@interface MainUInt : MainNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((objc_runtime_name("KotlinLong")))
__attribute__((swift_name("KotlinLong")))
@interface MainLong : MainNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((objc_runtime_name("KotlinULong")))
__attribute__((swift_name("KotlinULong")))
@interface MainULong : MainNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((objc_runtime_name("KotlinFloat")))
__attribute__((swift_name("KotlinFloat")))
@interface MainFloat : MainNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((objc_runtime_name("KotlinDouble")))
__attribute__((swift_name("KotlinDouble")))
@interface MainDouble : MainNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((objc_runtime_name("KotlinBoolean")))
__attribute__((swift_name("KotlinBoolean")))
@interface MainBoolean : MainNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("Captor")))
@protocol MainCaptor
@required
- (void)executeCallback:(id<MainCaptorCallback>)callback __attribute__((swift_name("execute(callback:)")));
@end;

__attribute__((swift_name("CaptorCallback")))
@protocol MainCaptorCallback
@required
- (void)onDataCapturedInput:(id<MainInput>)input __attribute__((swift_name("onDataCaptured(input:)")));
- (void)onFailureFailure:(id<MainFailure>)failure __attribute__((swift_name("onFailure(failure:)")));
@end;

__attribute__((swift_name("CaptorProvider")))
@protocol MainCaptorProvider
@required
- (id<MainCaptor> _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (void)setName:(NSString *)name captor:(id<MainCaptor>)captor __attribute__((swift_name("set(name:captor:)")));
@end;

__attribute__((swift_name("Failure")))
@protocol MainFailure
@required
@property (readonly) int32_t errorCode __attribute__((swift_name("errorCode")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FailureCompanion")))
@interface MainFailureCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (readonly) int32_t REASON_QR_CODE_CAPTURE_FAILURE __attribute__((swift_name("REASON_QR_CODE_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_DOCUMENT_CAPTURE_FAILURE __attribute__((swift_name("REASON_DOCUMENT_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_FACE_CAPTURE_FAILURE __attribute__((swift_name("REASON_FACE_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_FACE_CAPTURE_ABORT __attribute__((swift_name("REASON_FACE_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_DEVICE_ROOTED __attribute__((swift_name("REASON_DEVICE_ROOTED")));
@property (readonly) int32_t REASON_DEVICE_HOOKED __attribute__((swift_name("REASON_DEVICE_HOOKED")));
@property (readonly) int32_t REASON_DEVICE_SUSPICIOUS __attribute__((swift_name("REASON_DEVICE_SUSPICIOUS")));
@property (readonly) int32_t REASON_SECURITY_TAMPERED __attribute__((swift_name("REASON_SECURITY_TAMPERED")));
@end;

__attribute__((swift_name("FcmMessagingHandler")))
@protocol MainFcmMessagingHandler
@required
- (void)onMessageReceivedFrom:(NSString *)from data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("onMessageReceived(from:data:)")));
@end;

__attribute__((swift_name("Input")))
@protocol MainInput
@required
@property (readonly) MainMutableDictionary<NSString *, NSString *> *pages __attribute__((swift_name("pages")));
@end;

__attribute__((swift_name("Result")))
@protocol MainResult
@required
@property (readonly) NSString * _Nullable code __attribute__((swift_name("code")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly, getter=description_) MainMutableDictionary<NSString *, NSString *> * _Nullable description __attribute__((swift_name("description")));
@end;

__attribute__((swift_name("Session")))
@protocol MainSession
@required
- (void)start __attribute__((swift_name("start()")));
- (void)resumeTaskId:(NSString *)taskId __attribute__((swift_name("resume(taskId:)")));
- (void)updateTaskId:(NSString *)taskId input:(id<MainInput>)input __attribute__((swift_name("update(taskId:input:)")));
- (void)result __attribute__((swift_name("result()")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@property (readonly) NSMutableArray<id<MainTask>> *tasks __attribute__((swift_name("tasks")));
@property (readonly) MainStatus *status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol MainKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface MainKotlinEnum : KotlinBase <MainKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
- (int32_t)compareToOther:(MainKotlinEnum *)other __attribute__((swift_name("compareTo(other:)")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Status")))
@interface MainStatus : MainKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
@property (class, readonly) MainStatus *created __attribute__((swift_name("created")));
@property (class, readonly) MainStatus *initialized __attribute__((swift_name("initialized")));
@property (class, readonly) MainStatus *completed __attribute__((swift_name("completed")));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (int32_t)compareToOther:(MainStatus *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("Task")))
@protocol MainTask
@required
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSString * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface MainKotlinThrowable : KotlinBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (MainKotlinArray *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
@property (readonly) MainKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((swift_name("KotlinException")))
@interface MainKotlinException : MainKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiHttpFailure")))
@interface MainTdiHttpFailure : MainKotlinException <MainFailure>
- (instancetype)initWithErrorCode:(int32_t)errorCode errorMessage:(NSString *)errorMessage __attribute__((swift_name("init(errorCode:errorMessage:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (MainTdiHttpFailure *)doCopyErrorCode:(int32_t)errorCode errorMessage:(NSString *)errorMessage __attribute__((swift_name("doCopy(errorCode:errorMessage:)")));
@end;

__attribute__((swift_name("AbstractCaptorProviderImpl")))
@interface MainAbstractCaptorProviderImpl : KotlinBase <MainCaptorProvider>
- (instancetype)initWithCaptorFactory:(id<MainCaptorFactory>)captorFactory __attribute__((swift_name("init(captorFactory:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AbstractCaptorProviderImpl.Companion")))
@interface MainAbstractCaptorProviderImplCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((swift_name("AbstractSessionImpl")))
@interface MainAbstractSessionImpl : KotlinBase <MainSession>
- (instancetype)initWithCaptorProvider:(id<MainCaptorProvider>)captorProvider captorInvoker:(id<MainCaptorInvoker>)captorInvoker eventListener:(MainKotlinUnit *(^)(MainScenarioInfo *))eventListener __attribute__((swift_name("init(captorProvider:captorInvoker:eventListener:)"))) __attribute__((objc_designated_initializer));
- (void)initialize __attribute__((swift_name("initialize()")));
- (void)onEventReceivedScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("onEventReceived(scenarioInfo:)")));
- (MainSessionStateManager *)createSessionStateManager __attribute__((swift_name("createSessionStateManager()")));
- (id<MainPullTaskStateHandler>)createPullScenarioHandler __attribute__((swift_name("createPullScenarioHandler()")));
@property (readonly) id<MainCaptorProvider> captorProvider __attribute__((swift_name("captorProvider")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AbstractSessionImpl.Companion")))
@interface MainAbstractSessionImplCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InputImpl")))
@interface MainInputImpl : KotlinBase <MainInput>
- (instancetype)initWithPages:(MainMutableDictionary<NSString *, NSString *> *)pages __attribute__((swift_name("init(pages:)"))) __attribute__((objc_designated_initializer));
- (MainMutableDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (MainInputImpl *)doCopyPages:(MainMutableDictionary<NSString *, NSString *> *)pages __attribute__((swift_name("doCopy(pages:)")));
@end;

__attribute__((swift_name("CaptorFactory")))
@protocol MainCaptorFactory
@required
- (id<MainCaptor>)getQrCodeCaptor __attribute__((swift_name("getQrCodeCaptor()")));
- (id<MainCaptor>)getDocumentCaptor __attribute__((swift_name("getDocumentCaptor()")));
- (id<MainCaptor>)getA4DocumentCaptor __attribute__((swift_name("getA4DocumentCaptor()")));
- (id<MainCaptor>)getPassportCaptor __attribute__((swift_name("getPassportCaptor()")));
- (id<MainCaptor>)getIdDocumentCaptor __attribute__((swift_name("getIdDocumentCaptor()")));
- (id<MainCaptor>)getLivelinessFaceCaptor __attribute__((swift_name("getLivelinessFaceCaptor()")));
- (id<MainCaptor>)getKnomiLivenessCaptor __attribute__((swift_name("getKnomiLivenessCaptor()")));
@end;

__attribute__((swift_name("CaptorInvoker")))
@protocol MainCaptorInvoker
@required
- (void)executeCaptor:(id<MainCaptor>)captor listener:(MainKotlinUnit *(^)(id<MainInput>))listener __attribute__((swift_name("execute(captor:listener:)")));
@end;

__attribute__((swift_name("HttpApiFactory")))
@interface MainHttpApiFactory : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<MainTdiRequester>)getInitRequester __attribute__((swift_name("getInitRequester()")));
- (id<MainTdiRequester>)getStatePatcherScenarioId:(NSString *)scenarioId stepId:(NSString *)stepId statePatchRequest:(MainStatePatchRequest *)statePatchRequest __attribute__((swift_name("getStatePatcher(scenarioId:stepId:statePatchRequest:)")));
- (id<MainTdiRequester>)getScenarioRequesterScenarioId:(NSString *)scenarioId __attribute__((swift_name("getScenarioRequester(scenarioId:)")));
@property (readonly) NSString *scenarioName __attribute__((swift_name("scenarioName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HttpApiFactoryImpl")))
@interface MainHttpApiFactoryImpl : MainHttpApiFactory
- (instancetype)initWithBaseURL:(NSString *)baseURL scenarioName:(NSString *)scenarioName buildUtils:(MainBuildUtils *)buildUtils token:(NSString *)token correlationId:(NSString *)correlationId tenantId:(NSString *)tenantId pushTokenProvider:(id<MainPushTokenProvider>)pushTokenProvider __attribute__((swift_name("init(baseURL:scenarioName:buildUtils:token:correlationId:tenantId:pushTokenProvider:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
@end;

__attribute__((swift_name("PullTaskStateHandler")))
@protocol MainPullTaskStateHandler
@required
- (void)startPollingScenarioId:(NSString *)scenarioId eventListener:(MainKotlinUnit *(^)(MainScenarioInfo *))eventListener __attribute__((swift_name("startPolling(scenarioId:eventListener:)")));
- (void)stopPolling __attribute__((swift_name("stopPolling()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("PullTaskStateHandlerImpl")))
@interface MainPullTaskStateHandlerImpl : KotlinBase <MainPullTaskStateHandler>
- (instancetype)initWithHttpApiFactory:(MainHttpApiFactory *)httpApiFactory __attribute__((swift_name("init(httpApiFactory:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("PushTokenProvider")))
@protocol MainPushTokenProvider
@required
- (NSString * _Nullable)getToken __attribute__((swift_name("getToken()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiConstants")))
@interface MainTdiConstants : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tdiConstants __attribute__((swift_name("init()")));
@property (readonly) BOOL DEV_MODE __attribute__((swift_name("DEV_MODE")));
@property (readonly) BOOL SSL_PINNING __attribute__((swift_name("SSL_PINNING")));
@property (readonly) NSString *FINISHED_SUCCESS __attribute__((swift_name("FINISHED_SUCCESS")));
@property (readonly) NSString *FINISHED_FAILURE __attribute__((swift_name("FINISHED_FAILURE")));
@property (readonly) NSString *FINISHED_ERROR __attribute__((swift_name("FINISHED_ERROR")));
@property (readonly) NSString *WAITING __attribute__((swift_name("WAITING")));
@property (readonly) NSString *RUNNING __attribute__((swift_name("RUNNING")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IOSSessionImpl")))
@interface MainIOSSessionImpl : MainAbstractSessionImpl
- (instancetype)initWithToken:(NSString *)token captorProvider:(id<MainCaptorProvider>)captorProvider httpApiFactory:(MainHttpApiFactory *)httpApiFactory eventListener:(MainKotlinUnit *(^)(MainScenarioInfo *))eventListener captorInvoker:(id<MainCaptorInvoker>)captorInvoker __attribute__((swift_name("init(token:captorProvider:httpApiFactory:eventListener:captorInvoker:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaptorProvider:(id<MainCaptorProvider>)captorProvider captorInvoker:(id<MainCaptorInvoker>)captorInvoker eventListener:(MainKotlinUnit *(^)(MainScenarioInfo *))eventListener __attribute__((swift_name("init(captorProvider:captorInvoker:eventListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((swift_name("SessionStateManager")))
@interface MainSessionStateManager : KotlinBase
- (instancetype)initWithHttpApiFactory:(MainHttpApiFactory *)httpApiFactory __attribute__((swift_name("init(httpApiFactory:)"))) __attribute__((objc_designated_initializer));
- (MainKotlinPair *)onEventReceivedScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("onEventReceived(scenarioInfo:)")));
- (void)stop __attribute__((swift_name("stop()")));
@property (readonly) MainStatus *status __attribute__((swift_name("status")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IOSSessionStateManager")))
@interface MainIOSSessionStateManager : MainSessionStateManager
- (instancetype)initWithHttpApiFactory:(MainHttpApiFactory *)httpApiFactory __attribute__((swift_name("init(httpApiFactory:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResultAdapter")))
@interface MainResultAdapter : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)resultAdapter __attribute__((swift_name("init()")));
- (id<MainResult> _Nullable)adaptFromScenarioInfoScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("adaptFromScenarioInfo(scenarioInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SessionStateManager.Companion")))
@interface MainSessionStateManagerCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TaskAdapter")))
@interface MainTaskAdapter : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)taskAdapter __attribute__((swift_name("init()")));
- (NSMutableArray<id<MainTask>> * _Nullable)adaptFromScenarioInfoScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("adaptFromScenarioInfo(scenarioInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TaskImpl")))
@interface MainTaskImpl : KotlinBase <MainTask>
- (instancetype)initWithId:(NSString *)id name:(NSString * _Nullable)name status:(NSString * _Nullable)status category:(NSString * _Nullable)category __attribute__((swift_name("init(id:name:status:category:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (MainTaskImpl *)doCopyId:(NSString *)id name:(NSString * _Nullable)name status:(NSString * _Nullable)status category:(NSString * _Nullable)category __attribute__((swift_name("doCopy(id:name:status:category:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("BuildUtils")))
@interface MainBuildUtils : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property NSString *appId __attribute__((swift_name("appId")));
@property NSString *appVersionName __attribute__((swift_name("appVersionName")));
@property NSString *appAppVersionCode __attribute__((swift_name("appAppVersionCode")));
@property NSString *sdkVersionName __attribute__((swift_name("sdkVersionName")));
@end;

__attribute__((swift_name("TdiRequester")))
@protocol MainTdiRequester
@required
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequester")))
@interface MainInitRequester : KotlinBase <MainTdiRequester>
- (instancetype)initWithBaseURL:(NSString *)baseURL tenantId:(NSString *)tenantId headerMap:(NSDictionary<NSString *, NSString *> *)headerMap initRequest:(MainInitRequest *)initRequest __attribute__((swift_name("init(baseURL:tenantId:headerMap:initRequest:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequester.Companion")))
@interface MainInitRequesterCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiClient")))
@interface MainTdiClient : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tdiClient __attribute__((swift_name("init()")));
- (MainKtor_client_coreHttpClient *)getPlatformClient __attribute__((swift_name("getPlatformClient()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequest")))
@interface MainInitRequest : KotlinBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainInitRequest *)doCopyName:(NSString *)name __attribute__((swift_name("doCopy(name:)")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequest.Companion")))
@interface MainInitRequestCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<MainKotlinx_serialization_runtime_nativeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerializationStrategy")))
@protocol MainKotlinx_serialization_runtime_nativeSerializationStrategy
@required
- (void)serializeEncoder:(id<MainKotlinx_serialization_runtime_nativeEncoder>)encoder obj:(id _Nullable)obj __attribute__((swift_name("serialize(encoder:obj:)")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeDeserializationStrategy")))
@protocol MainKotlinx_serialization_runtime_nativeDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (id _Nullable)patchDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder old:(id _Nullable)old __attribute__((swift_name("patch(decoder:old:)")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeKSerializer")))
@protocol MainKotlinx_serialization_runtime_nativeKSerializer <MainKotlinx_serialization_runtime_nativeSerializationStrategy, MainKotlinx_serialization_runtime_nativeDeserializationStrategy>
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeGeneratedSerializer")))
@protocol MainKotlinx_serialization_runtime_nativeGeneratedSerializer <MainKotlinx_serialization_runtime_nativeKSerializer>
@required
- (MainKotlinArray *)childSerializers __attribute__((swift_name("childSerializers()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequest.$serializer")))
@interface MainInitRequest$serializer : KotlinBase <MainKotlinx_serialization_runtime_nativeGeneratedSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)$serializer __attribute__((swift_name("init()")));
- (MainInitRequest *)deserializeDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (MainInitRequest *)patchDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder old:(MainInitRequest *)old __attribute__((swift_name("patch(decoder:old:)")));
- (void)serializeEncoder:(id<MainKotlinx_serialization_runtime_nativeEncoder>)encoder obj:(MainInitRequest *)obj __attribute__((swift_name("serialize(encoder:obj:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatePatchRequest")))
@interface MainStatePatchRequest : KotlinBase
- (instancetype)initWithName:(NSString *)name input:(MainMutableDictionary<NSString *, NSString *> *)input __attribute__((swift_name("init(name:input:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainMutableDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (MainStatePatchRequest *)doCopyName:(NSString *)name input:(MainMutableDictionary<NSString *, NSString *> *)input __attribute__((swift_name("doCopy(name:input:)")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) MainMutableDictionary<NSString *, NSString *> *input __attribute__((swift_name("input")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatePatchRequest.Companion")))
@interface MainStatePatchRequestCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (id<MainKotlinx_serialization_runtime_nativeKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatePatchRequest.$serializer")))
@interface MainStatePatchRequest$serializer : KotlinBase <MainKotlinx_serialization_runtime_nativeGeneratedSerializer>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)$serializer __attribute__((swift_name("init()")));
- (MainStatePatchRequest *)deserializeDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
- (MainStatePatchRequest *)patchDecoder:(id<MainKotlinx_serialization_runtime_nativeDecoder>)decoder old:(MainStatePatchRequest *)old __attribute__((swift_name("patch(decoder:old:)")));
- (void)serializeEncoder:(id<MainKotlinx_serialization_runtime_nativeEncoder>)encoder obj:(MainStatePatchRequest *)obj __attribute__((swift_name("serialize(encoder:obj:)")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineFactory")))
@protocol MainKtor_client_coreHttpClientEngineFactory
@required
- (id<MainKtor_client_coreHttpClientEngine>)createBlock:(MainKotlinUnit *(^)(MainKtor_client_coreHttpClientEngineConfig *))block __attribute__((swift_name("create(block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ios")))
@interface MainIos : KotlinBase <MainKtor_client_coreHttpClientEngineFactory>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)ios __attribute__((swift_name("init()")));
- (id<MainKtor_client_coreHttpClientEngine>)createBlock:(MainKotlinUnit *(^)(MainIosClientEngineConfig *))block __attribute__((swift_name("create(block:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol MainKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<MainKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("Kotlinx_ioCloseable")))
@protocol MainKotlinx_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol MainKtor_client_coreHttpClientEngine <MainKotlinx_coroutines_coreCoroutineScope, MainKotlinx_ioCloseable>
@required
- (void)installClient:(MainKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) MainKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) MainKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IosClientEngine")))
@interface MainIosClientEngine : KotlinBase <MainKtor_client_coreHttpClientEngine>
- (instancetype)initWithConfig:(MainIosClientEngineConfig *)config __attribute__((swift_name("init(config:)"))) __attribute__((objc_designated_initializer));
@property (readonly) MainIosClientEngineConfig *config __attribute__((swift_name("config")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface MainKtor_client_coreHttpClientEngineConfig : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property MainKotlinx_coroutines_coreCoroutineDispatcher * _Nullable dispatcher __attribute__((swift_name("dispatcher")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property (readonly) MainKtor_client_coreHttpResponseConfig *response __attribute__((swift_name("response")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IosClientEngineConfig")))
@interface MainIosClientEngineConfig : MainKtor_client_coreHttpClientEngineConfig
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)configureRequestBlock:(MainKotlinUnit *(^)(NSMutableURLRequest *))block __attribute__((swift_name("configureRequest(block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IosHttpRequestException")))
@interface MainIosHttpRequestException : MainKotlinThrowable
- (instancetype)initWithOrigin:(NSError * _Nullable)origin __attribute__((swift_name("init(origin:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (readonly) NSError * _Nullable origin __attribute__((swift_name("origin")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logger")))
@interface MainLogger : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logger.Companion")))
@interface MainLoggerCompanion : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
- (void)debugTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("debug(tag:message:)")));
- (void)errorTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("error(tag:message:)")));
- (void)errorTag:(NSString *)tag message:(NSString *)message exception:(MainKotlinThrowable *)exception __attribute__((swift_name("error(tag:message:exception:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScenarioInfo")))
@interface MainScenarioInfo : KotlinBase
- (instancetype)initWithId:(NSString *)id name:(NSString * _Nullable)name state:(MainState * _Nullable)state status:(NSString *)status __attribute__((swift_name("init(id:name:state:status:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainState * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (MainScenarioInfo *)doCopyId:(NSString *)id name:(NSString * _Nullable)name state:(MainState * _Nullable)state status:(NSString *)status __attribute__((swift_name("doCopy(id:name:state:status:)")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) MainState * _Nullable state __attribute__((swift_name("state")));
@property (readonly) NSString *status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State")))
@interface MainState : KotlinBase
- (instancetype)initWithResult:(MainResult * _Nullable)result steps:(NSMutableArray<MainStep *> * _Nullable)steps __attribute__((swift_name("init(result:steps:)"))) __attribute__((objc_designated_initializer));
- (MainResult * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSMutableArray<MainStep *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainState *)doCopyResult:(MainResult * _Nullable)result steps:(NSMutableArray<MainStep *> * _Nullable)steps __attribute__((swift_name("doCopy(result:steps:)")));
@property (readonly) MainResult * _Nullable result __attribute__((swift_name("result")));
@property (readonly) NSMutableArray<MainStep *> * _Nullable steps __attribute__((swift_name("steps")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Result_")))
@interface MainResult : KotlinBase
- (instancetype)initWithCode:(NSString * _Nullable)code message:(NSString * _Nullable)message description:(MainMutableDictionary<NSString *, NSString *> * _Nullable)description __attribute__((swift_name("init(code:message:description:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainMutableDictionary<NSString *, NSString *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (MainResult *)doCopyCode:(NSString * _Nullable)code message:(NSString * _Nullable)message description:(MainMutableDictionary<NSString *, NSString *> * _Nullable)description __attribute__((swift_name("doCopy(code:message:description:)")));
@property (readonly) NSString * _Nullable code __attribute__((swift_name("code")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly, getter=description_) MainMutableDictionary<NSString *, NSString *> * _Nullable description __attribute__((swift_name("description")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Step")))
@interface MainStep : KotlinBase
- (instancetype)initWithDescription:(NSString * _Nullable)description id:(NSString *)id name:(NSString * _Nullable)name category:(NSString * _Nullable)category status:(NSString * _Nullable)status __attribute__((swift_name("init(description:id:name:category:status:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainStep *)doCopyDescription:(NSString * _Nullable)description id:(NSString *)id name:(NSString * _Nullable)name category:(NSString * _Nullable)category status:(NSString * _Nullable)status __attribute__((swift_name("doCopy(description:id:name:category:status:)")));
@property (readonly, getter=description_) NSString * _Nullable description __attribute__((swift_name("description")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSString * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface MainKotlinArray : KotlinBase
+ (instancetype)arrayWithSize:(int32_t)size init:(id _Nullable (^)(MainInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (id _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<MainKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(id _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface MainKotlinUnit : KotlinBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface MainKotlinPair : KotlinBase
- (instancetype)initWithFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)component1 __attribute__((swift_name("component1()")));
- (id _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainKotlinPair *)doCopyFirst:(id _Nullable)first second:(id _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) id _Nullable first __attribute__((swift_name("first")));
@property (readonly) id _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface MainKtor_client_coreHttpClient : KotlinBase <MainKotlinx_coroutines_coreCoroutineScope, MainKotlinx_ioCloseable>
- (instancetype)initWithEngine:(id<MainKtor_client_coreHttpClientEngine>)engine userConfig:(MainKtor_client_coreHttpClientConfig *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (MainKtor_client_coreHttpClient *)configBlock:(MainKotlinUnit *(^)(MainKtor_client_coreHttpClientConfig *))block __attribute__((swift_name("config(block:)")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) MainKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) id<MainKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) MainKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) MainKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) MainKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) MainKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) MainKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeEncoder")))
@protocol MainKotlinx_serialization_runtime_nativeEncoder
@required
- (id<MainKotlinx_serialization_runtime_nativeCompositeEncoder>)beginCollectionDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc collectionSize:(int32_t)collectionSize typeParams:(MainKotlinArray *)typeParams __attribute__((swift_name("beginCollection(desc:collectionSize:typeParams:)")));
- (id<MainKotlinx_serialization_runtime_nativeCompositeEncoder>)beginStructureDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc typeParams:(MainKotlinArray *)typeParams __attribute__((swift_name("beginStructure(desc:typeParams:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescription:(MainKotlinx_serialization_runtime_nativeEnumDescriptor *)enumDescription ordinal:(int32_t)ordinal __attribute__((swift_name("encodeEnum(enumDescription:ordinal:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<MainKotlinx_serialization_runtime_nativeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<MainKotlinx_serialization_runtime_nativeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
- (void)encodeUnit __attribute__((swift_name("encodeUnit()")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerialDescriptor")))
@protocol MainKotlinx_serialization_runtime_nativeSerialDescriptor
@required
- (NSArray<id<MainKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (NSArray<id<MainKotlinAnnotation>> *)getEntityAnnotations __attribute__((swift_name("getEntityAnnotations()")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) MainKotlinx_serialization_runtime_nativeSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeDecoder")))
@protocol MainKotlinx_serialization_runtime_nativeDecoder
@required
- (id<MainKotlinx_serialization_runtime_nativeCompositeDecoder>)beginStructureDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc typeParams:(MainKotlinArray *)typeParams __attribute__((swift_name("beginStructure(desc:typeParams:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescription:(MainKotlinx_serialization_runtime_nativeEnumDescriptor *)enumDescription __attribute__((swift_name("decodeEnum(enumDescription:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (MainKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
- (void)decodeUnit __attribute__((swift_name("decodeUnit()")));
- (id _Nullable)updateNullableSerializableValueDeserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableValue(deserializer:old:)")));
- (id _Nullable)updateSerializableValueDeserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableValue(deserializer:old:)")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) MainKotlinx_serialization_runtime_nativeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol MainKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<MainKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<MainKotlinCoroutineContextElement> _Nullable)getKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<MainKotlinCoroutineContext>)minusKeyKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<MainKotlinCoroutineContext>)plusContext:(id<MainKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol MainKotlinCoroutineContextElement <MainKotlinCoroutineContext>
@required
@property (readonly) id<MainKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface MainKotlinAbstractCoroutineContextElement : KotlinBase <MainKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol MainKotlinContinuationInterceptor <MainKotlinCoroutineContextElement>
@required
- (id<MainKotlinContinuation>)interceptContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface MainKotlinx_coroutines_coreCoroutineDispatcher : MainKotlinAbstractCoroutineContextElement <MainKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (void)dispatchContext:(id<MainKotlinCoroutineContext>)context block:(id<MainKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<MainKotlinCoroutineContext>)context block:(id<MainKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (BOOL)isDispatchNeededContext:(id<MainKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (MainKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(MainKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpResponseConfig")))
@interface MainKtor_client_coreHttpResponseConfig : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property MainKotlinx_ioCharset *defaultCharset __attribute__((swift_name("defaultCharset")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol MainKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface MainKtor_client_coreHttpClientConfig : KotlinBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (MainKtor_client_coreHttpClientConfig *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(MainKotlinUnit *(^)(MainKtor_client_coreHttpClientEngineConfig *))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(MainKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installFeature:(id<MainKtor_client_coreHttpClientFeature>)feature configure:(MainKotlinUnit *(^)(id))configure __attribute__((swift_name("install(feature:configure:)")));
- (void)installKey:(NSString *)key block:(MainKotlinUnit *(^)(MainKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(MainKtor_client_coreHttpClientConfig *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol MainKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(MainKtor_utilsAttributeKey *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("contains(key:)")));
- (id)getKey_:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(MainKtor_utilsAttributeKey *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(MainKtor_utilsAttributeKey *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<MainKtor_utilsAttributeKey *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface MainKtor_utilsPipeline : KotlinBase
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(MainKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));
- (void)insertPhaseAfterReference:(MainKtor_utilsPipelinePhase *)reference phase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(MainKtor_utilsPipelinePhase *)reference phase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(MainKtor_utilsPipelinePhase *)phase block:(id<MainKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (void)mergeFrom:(MainKtor_utilsPipeline *)from __attribute__((swift_name("merge(from:)")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<MainKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface MainKtor_client_coreHttpReceivePipeline : MainKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface MainKtor_client_coreHttpRequestPipeline : MainKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface MainKtor_client_coreHttpResponsePipeline : MainKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface MainKtor_client_coreHttpSendPipeline : MainKtor_utilsPipeline
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeCompositeEncoder")))
@protocol MainKotlinx_serialization_runtime_nativeCompositeEncoder
@required
- (void)encodeBooleanElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(desc:index:value:)")));
- (void)encodeByteElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(desc:index:value:)")));
- (void)encodeCharElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(desc:index:value:)")));
- (void)encodeDoubleElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(desc:index:value:)")));
- (void)encodeFloatElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(desc:index:value:)")));
- (void)encodeIntElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(desc:index:value:)")));
- (void)encodeLongElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(desc:index:value:)")));
- (void)encodeNonSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(id)value __attribute__((swift_name("encodeNonSerializableElement(desc:index:value:)")));
- (void)encodeNullableSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index serializer:(id<MainKotlinx_serialization_runtime_nativeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(desc:index:serializer:value:)")));
- (void)encodeSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index serializer:(id<MainKotlinx_serialization_runtime_nativeSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(desc:index:serializer:value:)")));
- (void)encodeShortElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(desc:index:value:)")));
- (void)encodeStringElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(desc:index:value:)")));
- (void)encodeUnitElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("encodeUnitElement(desc:index:)")));
- (void)endStructureDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc __attribute__((swift_name("endStructure(desc:)")));
- (BOOL)shouldEncodeElementDefaultDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(desc:index:)")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialModule> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerialClassDescImpl")))
@interface MainKotlinx_serialization_runtime_nativeSerialClassDescImpl : KotlinBase <MainKotlinx_serialization_runtime_nativeSerialDescriptor>
- (instancetype)initWithName:(NSString *)name generatedSerializer:(id<MainKotlinx_serialization_runtime_nativeGeneratedSerializer> _Nullable)generatedSerializer __attribute__((swift_name("init(name:generatedSerializer:)"))) __attribute__((objc_designated_initializer));
- (void)addElementName:(NSString *)name isOptional:(BOOL)isOptional __attribute__((swift_name("addElement(name:isOptional:)")));
- (void)pushAnnotationA:(id<MainKotlinAnnotation>)a __attribute__((swift_name("pushAnnotation(a:)")));
- (void)pushClassAnnotationA:(id<MainKotlinAnnotation>)a __attribute__((swift_name("pushClassAnnotation(a:)")));
- (void)pushDescriptorDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc __attribute__((swift_name("pushDescriptor(desc:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtime_nativeEnumDescriptor")))
@interface MainKotlinx_serialization_runtime_nativeEnumDescriptor : MainKotlinx_serialization_runtime_nativeSerialClassDescImpl
- (instancetype)initWithName:(NSString *)name choices:(MainKotlinArray *)choices __attribute__((swift_name("init(name:choices:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithName:(NSString *)name generatedSerializer:(id<MainKotlinx_serialization_runtime_nativeGeneratedSerializer> _Nullable)generatedSerializer __attribute__((swift_name("init(name:generatedSerializer:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerialModule")))
@protocol MainKotlinx_serialization_runtime_nativeSerialModule
@required
- (void)dumpToCollector:(id<MainKotlinx_serialization_runtime_nativeSerialModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<MainKotlinx_serialization_runtime_nativeKSerializer> _Nullable)getContextualKclass:(id<MainKotlinKClass>)kclass __attribute__((swift_name("getContextual(kclass:)")));
- (id<MainKotlinx_serialization_runtime_nativeKSerializer> _Nullable)getPolymorphicBaseClass:(id<MainKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<MainKotlinx_serialization_runtime_nativeKSerializer> _Nullable)getPolymorphicBaseClass:(id<MainKotlinKClass>)baseClass serializedClassName:(NSString *)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol MainKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerialKind")))
@interface MainKotlinx_serialization_runtime_nativeSerialKind : KotlinBase
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeCompositeDecoder")))
@protocol MainKotlinx_serialization_runtime_nativeCompositeDecoder
@required
- (BOOL)decodeBooleanElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(desc:index:)")));
- (int8_t)decodeByteElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeByteElement(desc:index:)")));
- (unichar)decodeCharElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeCharElement(desc:index:)")));
- (int32_t)decodeCollectionSizeDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc __attribute__((swift_name("decodeCollectionSize(desc:)")));
- (double)decodeDoubleElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(desc:index:)")));
- (int32_t)decodeElementIndexDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc __attribute__((swift_name("decodeElementIndex(desc:)")));
- (float)decodeFloatElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeFloatElement(desc:index:)")));
- (int32_t)decodeIntElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeIntElement(desc:index:)")));
- (int64_t)decodeLongElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeLongElement(desc:index:)")));
- (id _Nullable)decodeNullableSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index deserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableElement(desc:index:deserializer:)")));
- (id _Nullable)decodeSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index deserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableElement(desc:index:deserializer:)")));
- (int16_t)decodeShortElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeShortElement(desc:index:)")));
- (NSString *)decodeStringElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeStringElement(desc:index:)")));
- (void)decodeUnitElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index __attribute__((swift_name("decodeUnitElement(desc:index:)")));
- (void)endStructureDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc __attribute__((swift_name("endStructure(desc:)")));
- (id _Nullable)updateNullableSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index deserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateNullableSerializableElement(desc:index:deserializer:old:)")));
- (id _Nullable)updateSerializableElementDesc:(id<MainKotlinx_serialization_runtime_nativeSerialDescriptor>)desc index:(int32_t)index deserializer:(id<MainKotlinx_serialization_runtime_nativeDeserializationStrategy>)deserializer old:(id _Nullable)old __attribute__((swift_name("updateSerializableElement(desc:index:deserializer:old:)")));
@property (readonly) id<MainKotlinx_serialization_runtime_nativeSerialModule> context __attribute__((swift_name("context")));
@property (readonly) MainKotlinx_serialization_runtime_nativeUpdateMode *updateMode __attribute__((swift_name("updateMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface MainKotlinNothing : KotlinBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_runtime_nativeUpdateMode")))
@interface MainKotlinx_serialization_runtime_nativeUpdateMode : MainKotlinEnum
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
@property (class, readonly) MainKotlinx_serialization_runtime_nativeUpdateMode *banned __attribute__((swift_name("banned")));
@property (class, readonly) MainKotlinx_serialization_runtime_nativeUpdateMode *overwrite __attribute__((swift_name("overwrite")));
@property (class, readonly) MainKotlinx_serialization_runtime_nativeUpdateMode *update __attribute__((swift_name("update")));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (int32_t)compareToOther:(MainKotlinx_serialization_runtime_nativeUpdateMode *)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol MainKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol MainKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<MainKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol MainKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((swift_name("Kotlinx_ioCharset")))
@interface MainKotlinx_ioCharset : KotlinBase
- (instancetype)initWith_name:(NSString *)_name __attribute__((swift_name("init(_name:)"))) __attribute__((objc_designated_initializer));
- (MainKotlinx_ioCharsetDecoder *)doNewDecoder __attribute__((swift_name("doNewDecoder()")));
- (MainKotlinx_ioCharsetEncoder *)doNewEncoder __attribute__((swift_name("doNewEncoder()")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientFeature")))
@protocol MainKtor_client_coreHttpClientFeature
@required
- (void)installFeature:(id)feature scope:(MainKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (id)prepareBlock:(MainKotlinUnit *(^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) MainKtor_utilsAttributeKey *key __attribute__((swift_name("key")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface MainKtor_utilsAttributeKey : KotlinBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface MainKtor_utilsPipelinePhase : KotlinBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinSuspendFunction")))
@protocol MainKotlinSuspendFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol MainKotlinSuspendFunction2 <MainKotlinSuspendFunction>
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_runtime_nativeSerialModuleCollector")))
@protocol MainKotlinx_serialization_runtime_nativeSerialModuleCollector
@required
- (void)contextualKClass:(id<MainKotlinKClass>)kClass serializer:(id<MainKotlinx_serialization_runtime_nativeKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<MainKotlinKClass>)baseClass actualClass:(id<MainKotlinKClass>)actualClass actualSerializer:(id<MainKotlinx_serialization_runtime_nativeKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol MainKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol MainKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol MainKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol MainKotlinKClass <MainKotlinKDeclarationContainer, MainKotlinKAnnotatedElement, MainKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((swift_name("Kotlinx_ioCharsetDecoder")))
@interface MainKotlinx_ioCharsetDecoder : KotlinBase
- (instancetype)initWith_charset:(MainKotlinx_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Kotlinx_ioCharsetEncoder")))
@interface MainKotlinx_ioCharsetEncoder : KotlinBase
- (instancetype)initWith_charset:(MainKotlinx_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

NS_ASSUME_NONNULL_END
