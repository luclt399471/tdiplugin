package cordova.plugin.tdi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aware.face_liveness.api.FaceLiveness;
import applicationId.R;

import org.apache.cordova.CallbackContext;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.WRITE_SETTINGS;

//import com.aware.knomisclient.fragments.ActionButtonFragment;
//import com.aware.knomisclient.fragments.LivenessFragment;
//import com.aware.knomisclient.rest.RestClientTask;

public class SplashActivity extends AppCompatActivity {

    private static CallbackContext callbackContext;

    private String WORKFLOW = "Charlie4";
    private String USERNAME = "DemoThales";

    private static final String[] REQUESTED_PERMISSIONS = {CAMERA,WRITE_SETTINGS,INTERNET};
    private static final String[] PERMISSIONS = {CAMERA,INTERNET};

    // permission result codes
    private static final int CAMERA_PERM_RESULT = 100;
    private static final int WRITE_SETTINGS_PERM_RESULT = 300;
    private static final int INTERNET_PERM_RESULT = 400;

    private static final int ALL_PERMISSIONS_RESULT = 999;
    private static final int CODE_WRITE_SETTINGS_PERMISSION = 1300;  // special case write_settings
    private static int[] REQUESTED_PERMISSION_CODES = {
            CAMERA_PERM_RESULT, WRITE_SETTINGS_PERM_RESULT, INTERNET_PERM_RESULT
    };

    private boolean oneTimeAsk = false;

    private Logger logger = LoggerFactory.getLogger("SplashActivity");
    private TextView splashText;
    private TextView errorText;
    private ProgressBar progressbar;
//    private lateinit var baseManager: BaseManager
    private Handler mDelayHandler = new Handler();



    public static void initialize(Activity activity, CallbackContext cContext) {//, String faceChoice){
        callbackContext = cContext;
        Intent intent = new Intent(activity, SplashActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        oneTimeAsk=false;

        splashText = findViewById(R.id.splashtext);
        errorText = findViewById(R.id.errorMsg);
        progressbar = findViewById(R.id.progressPaymentBar);

        initUI();

    }

    @Override
    protected void onResume() {
        super.onResume();
            if(checkPermissionsAndSelectItem()) {
                requestPermission();
            } else{
                oneTimeAsk = true;
                errorText.setText("Missing Permission");
                errorText.setVisibility(View.VISIBLE);
                progressbar.setVisibility(View.GONE);
                splashText.setVisibility(View.GONE);

                return;
            }
    }

    private void initUI() {
        errorText.setVisibility(View.INVISIBLE);
        progressbar.setVisibility(View.VISIBLE);
        splashText.setVisibility(View.VISIBLE);
        //splashText.text = baseManager.getBrandingAppName()
        splashText.animate().alpha(0f).setDuration(1);
        splashText.animate().alpha(1f).setDuration(2000);
    }

    private boolean checkPermissionsAndSelectItem() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            ArrayList<String> request = findUnAskedPermissions(REQUESTED_PERMISSIONS);

            int code = ALL_PERMISSIONS_RESULT;
            if (request.size() > 0) {
                if (request.size() == 1) {
                    for (int i = 0; i < request.size(); i++) {
                        if (request.get(i).equals(REQUESTED_PERMISSIONS[i])) {
                            code = REQUESTED_PERMISSION_CODES[i];
                            break;
                        }
                    }
                }
//                mNextFragmentToRun = position;
                if (request.contains(WRITE_SETTINGS) && !Settings.System.canWrite(this)) {
                    // we must be >= M if here
//                    mNextFragmentToRun = position;
                    if (!oneTimeAsk) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, CODE_WRITE_SETTINGS_PERMISSION);
                    }
                    requestPermission();
                    return false;
                }
                return true;
            }
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_SETTINGS}, CODE_WRITE_SETTINGS_PERMISSION);
            return false;
        }
        return true;
    }

    private ArrayList<String> findUnAskedPermissions(String[] wanted) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean hasPermission(String permission) {
        // not using convenience method #usingRuntimePermissions to suppress lint errors
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        mNextFragmentToRun = NO_FRAGMENT_TO_RUN;

        if(requestCode == ALL_PERMISSIONS_RESULT){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                CaptureFaceActivity.initialize((Activity) this, callbackContext);
            }else {
                Toast.makeText(SplashActivity.this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(SplashActivity.this,PERMISSIONS, ALL_PERMISSIONS_RESULT);
    }
}
