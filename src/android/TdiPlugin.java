package cordova.plugin.tdi;

import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Mcid plugin wrapping the mcid SDK.
 */
public class TdiPlugin extends CordovaPlugin {

    private McidSDKIntegration mcidSDKIntegration;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();

        mcidSDKIntegration = new McidSDKIntegration(cordova.getActivity());
    }

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("init")) {
            Log.i("TdiPlugin..... ","Mcid init");
            mcidSDKIntegration.init(callbackContext);
            return true;
        } else if (action.equals("setDetectionZone")) {
            int var1 = data.getInt(0);
            double var2 = data.getDouble(1);
            mcidSDKIntegration.setDetectionZone(var1, var2, callbackContext);
        } else if (action.equals("setCaptureDocuments")) {
            mcidSDKIntegration.setCaptureDocuments(data.getString(0), callbackContext);
        } else if (action.equals("setCaptureNewDocument")) {
            mcidSDKIntegration.setCaptureNewDocument(data.getDouble(0), data.getDouble(1), data.getInt(2), callbackContext);
        } else if (action.equals("setAutoSnapshot")) {
            mcidSDKIntegration.setAutoSnapshot(data.getBoolean(0), callbackContext);
        } else if (action.equals("setEdgeDetectionTimeout")) {
            mcidSDKIntegration.setEdgeDetectionTimeout(data.getInt(0), callbackContext);
        } else if (action.equals("setAutocropping")) {
            mcidSDKIntegration.setAutocropping(data.getBoolean(0), callbackContext);
        } else if (action.equals("hideUIElementsForStep")) {
            mcidSDKIntegration.hideUIElementsForStep(data.getString(0), callbackContext);
        } else if (action.equals("setQACheckResultTimeout")) {
            mcidSDKIntegration.setQACheckResultTimeout(data.getInt(0), callbackContext);
        } else if (action.equals("setSecondPageTimeout")) {
            mcidSDKIntegration.setSecondPageTimeout(data.getInt(0), callbackContext);
        }else if (action.equals("initFaceCapture")) {
            Log.i("TdiPlugin ..... ","initFaceCapture");
            mcidSDKIntegration.initFaceCapture(callbackContext);
            return true;
        }else if (action.equals("initTDI")) {
            Log.i("TdiPlugin ..... ","initTDI");
            mcidSDKIntegration.initTDI(callbackContext);
            return true;
        }else if (action.equals("setTDIConfig")) {
            Log.i("TdiPlugin ..... ","setTDIConfig");
            mcidSDKIntegration.setTDIConfig(data.getString(0), data.getString(1), data.getString(2), data.getString(3), callbackContext);
            return true;
        }else if (action.equals("setTDIConfig")) {
            Log.i("TdiPlugin ..... ","setTDIConfig");
            mcidSDKIntegration.setTDIConfig(data.getString(0), data.getString(1), data.getString(2), data.getString(3), callbackContext);
            return true;
        }else if (action.equals("setLivenessConfig")) {
            Log.i("TdiPlugin ..... ","setLivenessConfig");
            mcidSDKIntegration.setLivenessConfig(data.getString(0), data.getString(1), callbackContext);
            return true;
        }else if (action.equals("setTextColor")) {
            Log.i("TdiPlugin ..... ","setTextColor");
            mcidSDKIntegration.setTextColor(data.getString(0), callbackContext);
            return true;
        }else if (action.equals("setBackgroundColor")) {
            Log.i("TdiPlugin ..... ","setBackgroundColor");
            mcidSDKIntegration.setBackgroundColor(data.getString(0), callbackContext);
            return true;
        }else if (action.equals("setTextType")) {
            Log.i("TdiPlugin ..... ","setTextType");
            mcidSDKIntegration.setTextType(data.getString(0), data.getInt(1), callbackContext);
            return true;
        }else if (action.equals("setFeedbackBackgroundColor")) {
            Log.i("TdiPlugin ..... ","setFeedbackBackgroundColor");
            mcidSDKIntegration.setFeedbackBackgroundColor(data.getString(0), callbackContext);
            return true;
        }
        return false;
    }
}