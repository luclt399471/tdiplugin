package cordova.plugin.tdi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import org.apache.cordova.CallbackContext;

public class McidSDKIntegration extends AppCompatActivity {


    private String docChoice = "ID card";
    //milliseconds
    private int edgeDetectionTimeout;
    private Boolean autoSnapshot;
    private Boolean autoCropping;
    private String uIElementsForStep;
    private int QACheckResultTimeout;
    private int SecondPageTimeout;
    private double[] newDoc;
    private double[] detectionZone;

    private final Context mContext;

    public McidSDKIntegration(final Context context) {
        mContext = context;
    }

    public void init(@NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            CaptureDocActivity.initialize((Activity) mContext, docChoice, callbackContext);
//            CaptureFaceActivity.initialize((Activity) mContext, callbackContext);
        }
    }

    public void initFaceCapture(@NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            SplashActivity.initialize((Activity) mContext, callbackContext);
        }
    }

    public void setDetectionZone(@NonNull final double var1, @NonNull final Double var2, @NonNull final CallbackContext callbackContext) {
        detectionZone = new double[]{var1,var2};
        CaptureDocActivity.detectionZone = detectionZone;
    }

    public void setCaptureDocuments(@NonNull final String dChoice, @NonNull final CallbackContext callbackContext) {
        docChoice = dChoice;
    }

    public void setCaptureNewDocument(@NonNull final double width, @NonNull final double height, @NonNull final int pages, @NonNull final CallbackContext callbackContext) {
        newDoc = new double[]{width,height,pages};
        CaptureDocActivity.newDoc = newDoc;
    }

    public void setAutoSnapshot(@NonNull final Boolean setAutoSnapshot, @NonNull final CallbackContext callbackContext) {
        autoSnapshot = setAutoSnapshot;
        CaptureDocActivity.autoSnapshot = autoSnapshot;
    }

    public void setEdgeDetectionTimeout(@NonNull final int timeout, @NonNull final CallbackContext callbackContext) {
        edgeDetectionTimeout = timeout;
        CaptureDocActivity.edgeDetectionTimeout = edgeDetectionTimeout;
    }

    public void setAutocropping(@NonNull final Boolean autocrop, @NonNull final CallbackContext callbackContext) {
        autoCropping = autocrop;
        CaptureDocActivity.autoCropping = autoCropping;
    }

    public void hideUIElementsForStep(@NonNull final String step, @NonNull final CallbackContext callbackContext) {
        uIElementsForStep = step;
        CaptureDocActivity.uIElementsForStep=uIElementsForStep;
    }

    public void setQACheckResultTimeout(@NonNull final int timeout, @NonNull final CallbackContext callbackContext) {
        QACheckResultTimeout = timeout;
        CaptureDocActivity.QACheckResultTimeout = QACheckResultTimeout;
    }

    public void setSecondPageTimeout(@NonNull final int timeout, @NonNull final CallbackContext callbackContext) {
        SecondPageTimeout = timeout;
        CaptureDocActivity.SecondPageTimeout = SecondPageTimeout;
    }
    
    public void setTDIConfig(@NonNull final String SCENARIO_NAME, @NonNull final String BASE_URL, @NonNull final String JWT_TOKEN, @NonNull final String TenantId, @NonNull final CallbackContext callbackContext) {
//        TDIActivity.Companion.setSCENARIO_NAME(SCENARIO_NAME);
//        TDIActivity.Companion.setBASE_URL(BASE_URL);
//        TDIActivity.Companion.setJWT_TOKEN(JWT_TOKEN);
//        TDIActivity.Companion.setTenantId(TenantId);
    }

    public void initTDI(@NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
//            TDIActivity.Companion.initialize((Activity) mContext, callbackContext);
        }
    }

    public void setLivenessConfig(@NonNull final String workflow, @NonNull final String username, @NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            CaptureFaceActivity.WORKFLOW = workflow;
            CaptureFaceActivity.USERNAME = username;
        }
    }

	public void setTextColor(@NonNull final String color, @NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            LivenessFragment.txtColor = color;
        }
    }
    public void setBackgroundColor(@NonNull final String color, @NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            LivenessFeedbackView.bgColor = color;
        }
    }
    public void setTextType(@NonNull final String family, @NonNull final int size, @NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            LivenessFragment.txtFamily = family;
            LivenessFragment.txtStyle = size;
        }
    }
    public void setFeedbackBackgroundColor(@NonNull final String color, @NonNull final CallbackContext callbackContext) {
        if (mContext instanceof Activity) {
            LivenessFragment.fbBgColor = color;
        }
    }
}
