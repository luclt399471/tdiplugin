package cordova.plugin.tdi

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.gemalto.ekyc.facez_capture.FaceCaptureSDK
import com.gemalto.tdi.*
import applicationId.R
import kotlinx.coroutines.runBlocking
import org.apache.cordova.CallbackContext
import org.apache.cordova.PluginResult
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable

class TDIActivity : AppCompatActivity(), (Status,MutableList<Task>?,Result?) -> Unit, Captor.Callback {
    private val logger = LoggerFactory.getLogger(TDIActivity::class.java)
    private var session: Session? = null
    private var tasks: ArrayList<Task> = ArrayList()
    private lateinit var txtCode: TextView
    private lateinit var txtMessage: TextView
    private lateinit var txtName: TextView
    private lateinit var txtDocVerif: TextView
    private lateinit var txtFaceVerif: TextView
    private lateinit var txtLivelinessVerif: TextView

    private val callbackContext: CallbackContext? = null

    companion object {

        var SCENARIO_NAME: String = ""
        var tenantId: String = ""
        var BASE_URL: String = ""
        var JWT_TOKEN: String = ""

//        var SCENARIO_NAME: String = "Demo3-78066b82-52b2-4766-b792-99627802ea9e"
//        var tenantId: String = ""
//        var BASE_URL: String = "https://apidemo.tdieu.ew1.msi-dev.acloud.gemalto.com"
//        var JWT_TOKEN: String = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjdjMGIyZTE4LTFmMzItNDM3Zi04ODFjLTJiZmNjZTRiZWI4OCJ9.eyJhdWQiOiJUREktRGVtby1UaGFsZXMiLCJyb2xlcyI6WyJzY3M6ZXhlY3V0ZVNjZW5hcmlvIl0sImlzcyI6ImRlbW8tdGhhbGVzLXJuZCIsImV4cCI6MTYwOTkwMzAwMiwianRpIjoiMjA4NzQyZjYtMzBmYy0xMWVhLTk0ZjMtNTQwNTMwOTM0ZjBiIn0.oE0-iG73Nb_ZMrTCeklX3iGfiBK8mB0EDJSFJqlIYP6V6hLOPdr6qb31qz_QI7QzY84FI90xWfg53_CHCN_gsQ"

        private var callbackContext: CallbackContext? = null

        fun initialize(activity: Activity, cContext: CallbackContext?) { //, String faceChoice){
            callbackContext = cContext
            val intent = Intent(activity, TDIActivity::class.java)
            activity.startActivity(intent)
        }

    }

    private val REQUESTED_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_SETTINGS,
        Manifest.permission.INTERNET,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val CODE_WRITE_SETTINGS_PERMISSION = 1300  // special case write_settings
    private val ALL_PERMISSIONS_RESULT = 999

    private val CAMERA_PERM_RESULT = 100
    private val WRITE_SETTINGS_PERM_RESULT = 300
    private val INTERNET_PERM_RESULT = 400
    private val REQUESTED_PERMISSION_CODES = intArrayOf(CAMERA_PERM_RESULT, WRITE_SETTINGS_PERM_RESULT, INTERNET_PERM_RESULT)

    override fun onDataCaptured(input: Input) {
        Toast.makeText(this, "Data captured successfully", Toast.LENGTH_LONG).show()
    }

    override fun onFailure(failure: Failure) {
        logger.error("$failure")
        Toast.makeText(this, "Failed to capture: $failure", Toast.LENGTH_LONG).show()
    }

    override operator fun invoke(status: Status, task: MutableList<Task>?,result: Result?) {
        logger.debug("onTaskRequested: $task")
        logger.debug("Current session state:[$status]")
        when (status) {
            Status.INITIALIZED -> {
                logger.debug("Execute task if any or waiting")
                task?.let {
                    tasks.clear()
                    tasks.addAll(it)
                }
            }
            Status.CREATED -> {
                logger.debug("New Session created and nothing to do")
                task?.let {
                    tasks.clear()
                    tasks.addAll(it)
                }
            }
            Status.COMPLETED -> {
                logger.debug("No more task to execute")
                result?.let {
                    if (it.code == "0" && it.description == null) {
                        runBlocking {
                            session?.result()
                        }
                    }
                }
            }
        }
        result?.let {
            logger.debug("${result.description}")
            runOnUiThread {
                txtCode.text = it.code
                txtMessage.text = it.message
                logger.debug("Full", "${it.description}")
                txtName.text = "Name = ${it.description?.get("firstName")}"
                txtDocVerif.text = "Document = ${it.description?.get("docVerifResultStatus")}"
                txtFaceVerif.text = "Face = ${it.description?.get("faceVerifResultStatus")}"
                txtLivelinessVerif.text = "Liveliness = ${it.description?.get("livelinessVerifResultStatus")}"

//                // Preserve callback
//                // Preserve callback
//                val pluginResult = PluginResult(PluginResult.Status.OK, "TDI SDK Session stop.")
//                pluginResult.keepCallback = true
//                callbackContext?.sendPluginResult(pluginResult)
//
//
////                val metaData: Map<String?, String?> = captureResult.getMetadata().getMetadata()
//                val result: MutableMap<String, String>? = it.description
//
//                try {
//                    val jsonObject = JSONObject(result)
//                    val jsonObject2 = JSONObject()
//                    jsonObject2.put("code", it.code)
//                    jsonObject2.put("message", it.message)
//                    jsonObject2.put("description", jsonObject)
//                    callbackContext?.success(jsonObject2)
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissionsAndSelectItem()
//        checkPermissionsAndSelectItem()
        if(session == null)
        TdiMobileSdk.initialize(this)
//        faceLicencing()
        val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
       // ActivityCompat.requestPermissions(this, permissions, 1)
        txtCode = findViewById(R.id.txtCode)
        txtMessage = findViewById(R.id.txtMessage)
        txtName = findViewById(R.id.txtName)
        txtDocVerif = findViewById(R.id.txtDocVerif)
        txtFaceVerif = findViewById(R.id.txtFaceVerif)
        txtLivelinessVerif = findViewById(R.id.txtLivelinessVerif)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) { }
                else {
                    Toast.makeText(this@TDIActivity, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun onVerificationClicked(view: View) {
        val tdiMobileSdk = TdiMobileSdk.getInstance()

        //tdiMobileSdk.captorProvider["TestCaptor"] = TestCaptor(this)

        //tdiMobileSdk.captorProvider?.get("blink")?.execute(this)

        logger.debug("GM hello")
        tdiMobileSdk.taskStateListener = this
       // Thread{
            try {
                //JWT Token
                   session = tdiMobileSdk.newSession(BASE_URL, SCENARIO_NAME,JWT_TOKEN, tenantId,"")
               // session = tdiMobileSdk.newSession("Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJlQ2FyZVQiLCJqdGkiOiJRWEJEeU9UZi1FdUxqLWNWODctY1ZnWXFEREJrbHhBIiwiZXhwIjoxNTU3NDg0NTg1LCJhdWQiOiJUREktTGVnaXRpbWF0aW9uLXNydiIsInJvbGVzIjoiTGVnaXRpbWF0aW9uLUNsZXJrIn0.Pbi18FxIYTMdUKK55cDEcp8ZCdDv-sPLoejBj37GEaCdCB4HwmOLB9pYoCMNiWgrNGiAs4-S03eUOqB8Gd54cA", "")

                session?.start()

               // session?.resume()
            } catch (e: Exception) {
                logger.error("${e.message}")
                runOnUiThread {
                    Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
                }
            } catch (e: TdiHttpFailure) { //OR TdiHttpFailure to access errorCode and errorMessage as variable
                logger.error("${e.errorCode}")
                logger.error(e.errorMessage)
                runOnUiThread {
                    Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
                }
            }


    }

    fun onStopSessionClicked(view: View){
        tasks.clear()
        session?.stop()
        runOnUiThread {
            Toast.makeText(this, "Session have been stopped", Toast.LENGTH_LONG).show()
        }
    }

    fun onGetScenarioClicked(view: View){
        try {
            session?.result()
        } catch (e: Exception) {
            logger.error("${e.message}")
            runOnUiThread {
                Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun onCapturePassportClicked(view: View) {
      //  TdiMobileSdk.getInstance().captorProvider["Passport"]?.execute(this)
        tasks.singleOrNull{
            it.category == "Passport"
        }?.let {
                try {
                    session?.resume(it.id)
                } catch (e: IllegalStateException) {
                    logger.error("${e.message}")
                    runOnUiThread {
                        Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG)
                            .show()
                    }
                }
        }?:run{
            logger.error("No Task Found")
        }
    }

    fun onCaptureIdDocumentClicked(view: View) {
        tasks.singleOrNull{
            it.category == "ID Card"
        }?.let {
            Thread {
                try {
                    session?.resume(it.id)
                } catch (e: Exception) {
                    logger.error("${e.message}")
                    runOnUiThread {
                        Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
                    }
                }
            }.start()
        }?:run{
            logger.error("No Task Found")
        }
    }

    fun onCaptureLivelinessFaceClicked(view: View) {
        //TdiMobileSdk.getInstance().captorProvider["AwareLiveness"]?.execute(this)
      /*  TdiMobileSdk.getInstance().captorProvider["AwareLiveness"]?.let {
            TdiMobileSdk.getInstance().captorProvider["Selfie"] = it
        }*/

        tasks.singleOrNull{
            it.category == "AwareLiveness"
        }?.let {
            Thread {
                try {
                    session?.resume("waitForSelfieAware")
                } catch (e: Exception) {
                    logger.error("${e.message}")
                    runOnUiThread {
                        Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
                    }
                }
            }.start()
        }?:run{
            logger.error("No Task Found")
        }
    }

    fun onCaptureA4DocumentClicked(view: View) {
        try {
            TdiMobileSdk.getInstance().captorProvider["AwareLiveness"]?.execute(this)
        }catch (e: Exception){
            logger.error("${e.message}")
            runOnUiThread {
                Toast.makeText(this, "Failed to resume: ${e.message}", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun onGetResultClicked(view: View) {
        txtCode.text = ""
        txtMessage.text = ""
        txtName.text = ""
        txtDocVerif.text = ""
        txtFaceVerif.text = ""
        txtLivelinessVerif.text = ""
        session?.result()
//        session?.stop()
    }

    private fun checkPermissionsAndSelectItem() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val request = findUnAskedPermissions(REQUESTED_PERMISSIONS)
            var code = ALL_PERMISSIONS_RESULT
            if (request.size > 0) {
                if (request.size == 1) {
                    for (i in request.indices) {
                        if (request.get(i) == REQUESTED_PERMISSIONS[i]) {
                            code = REQUESTED_PERMISSION_CODES[i]
                            break
                        }
                    }
                }
                if (request.contains(Manifest.permission.WRITE_SETTINGS) && !Settings.System.canWrite(this)) {
                    // we must be >= M if here
                    val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
                   // intent.data = Uri.parse("package:$packageName")
                    startActivityForResult(intent, CODE_WRITE_SETTINGS_PERMISSION)
                    return
                }
                requestPermissions(request.toTypedArray(), code)

                return
            }
        } else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_SETTINGS
            ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_SETTINGS), CODE_WRITE_SETTINGS_PERMISSION)
            return
        }
    }

    private fun findUnAskedPermissions(wanted: Array<String>): java.util.ArrayList<String> {
        val result = java.util.ArrayList<String>()

        for (perm in wanted) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }
        return result
    }

    private fun hasPermission(permission: String): Boolean {
        // not using convenience method #usingRuntimePermissions to suppress lint errors
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) === PackageManager.PERMISSION_GRANTED
    }

}