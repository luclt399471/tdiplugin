package cordova.plugin.tdi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import applicationId.R;

import net.gemalto.mcidsdk.CaptureListener;
import net.gemalto.mcidsdk.CaptureResult;
import net.gemalto.mcidsdk.CaptureSDK;
import net.gemalto.mcidsdk.InitListener;
import net.gemalto.mcidsdk.Metadata;
import net.gemalto.mcidsdk.ui.MkycSdkFragment;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;

import java.util.EnumSet;
import java.util.Map;

import static net.gemalto.mcidsdk.ui.Document.DocumentModeIdDocument;
import static net.gemalto.mcidsdk.ui.Document.DocumentModePassport;
import static net.gemalto.mcidsdk.ui.Document.DocumentModeICAO;
import net.gemalto.mcidsdk.ui.Document;
import java.util.Collections;

public class CaptureDocActivity extends AppCompatActivity
        implements CaptureListener, CaptureListener.DetectionWarningListener {

    private static final String TAG = "CAPTURE_DOC_ACTIVITY";
    private CaptureSDK captureSdk;
    private static CallbackContext callbackContext;

    public static Integer edgeDetectionTimeout;
    public static Boolean autoSnapshot;
    public static Boolean autoCropping;
    public static String uIElementsForStep;
    public static Integer QACheckResultTimeout;
    public static Integer SecondPageTimeout;
    public static double[] detectionZone;
    public static double[] newDoc;

    private TextView mQaLabels;

    public static void initialize(Activity activity, @NonNull String docChoice, CallbackContext cContext) {//, String faceChoice){
        callbackContext = cContext;
        Intent intent = new Intent(activity, CaptureDocActivity.class);
        intent.putExtra("docChoice", docChoice);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_doc);
        mQaLabels = findViewById(R.id.qa_labels);
        if (mQaLabels != null) mQaLabels.setVisibility(View.INVISIBLE);

        MkycSdkFragment docCaptureFragment = (MkycSdkFragment) getSupportFragmentManager().findFragmentById(R.id.capture_view);
        if (docCaptureFragment == null) {
            callbackContext.error("No fragment to get SDK");
            return;
        }
        captureSdk = docCaptureFragment.getSDK();

        //Set new document to capture
        if (newDoc != null) {
            float width = (float) newDoc[0];
            float height = (float) newDoc[1];
            int pages = (int)newDoc[2];

            Document doc = new Document(width, height, pages);
            captureSdk.setCaptureDocuments(Collections.singletonList(doc));
        }

        //Set Capture Document
        switch (getIntent().getStringExtra("docChoice")) {
            case "Passport":
                captureSdk.setCaptureDocuments(DocumentModePassport);
                break;
            case "ID card":
                captureSdk.setCaptureDocuments(DocumentModeIdDocument);
                break;
            case "CAO":
                captureSdk.setCaptureDocuments(DocumentModeICAO);
                break;
            default:
                Log.e("", "no case");
                callbackContext.error("No documents case");
                return;
        }

        //Set Detection Zone
        if (detectionZone != null) {
            int var1 = (int) detectionZone[0];
            double var2 = detectionZone[1];
            captureSdk.setDetectionZone(var1, var2);
        }

        //Set edgeDetectionTimeout
        if (edgeDetectionTimeout != null) {
            captureSdk.setEdgeDetectionTimeout(edgeDetectionTimeout);
        }

        //set autoSnapshot
        if (autoSnapshot != null) {
            captureSdk.setAutoSnapshot(autoSnapshot);
        }

        //Set autoCropping
        if (autoCropping != null) {
            captureSdk.setAutoCropping(autoCropping);
        }

        //set uIElementsForStep
        if (uIElementsForStep != null ) {
            switch (uIElementsForStep) {
                case "Detecting":
                    captureSdk.hideUIElementsForStep(CaptureSDK.CaptureStep.Detecting);
                    break;
                case "Cropping":
                    captureSdk.hideUIElementsForStep(CaptureSDK.CaptureStep.Cropping);
                    break;
                case "ResultOK":
                    captureSdk.hideUIElementsForStep(CaptureSDK.CaptureStep.ResultOK);
                    break;
                case "ResultKO":
                    captureSdk.hideUIElementsForStep(CaptureSDK.CaptureStep.ResultKO);
                    break;
                default:
                    break;
            }
        }

        //Set QACheckResultTimeout
        if (QACheckResultTimeout != null) {
            captureSdk.setQACheckResultTimeout(QACheckResultTimeout);
        }

        //Set SecondPageTimeout
        if (SecondPageTimeout != null) {
            captureSdk.setSecondPageTimeout(SecondPageTimeout);
        }

        captureSdk.setDetectionWarningsListener(this);

        //Mcid Sdk init
        captureSdk.init(new InitListener() {
            @Override
            public void onInitSuccess(boolean isCompleted, int errorCode) {
                if (isCompleted) {
                    // Preserve Mcid sdk init success callback
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "Mcid sdk init Success");
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Mcid Sdk start
                            captureSdk.start(CaptureDocActivity.this);
                        }
                    }, 4 * 1000);
                } else {
                    Log.d(TAG, String.format("Error on init (%d)", errorCode));
                    // Preserve Mcid sdk init error callback
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, "Mcid sdk init failed! " + errorCode);
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                }
            }
        });
    }

    @Override
    public void onSuccess(byte[] frontImage, byte[] backImage, Metadata metadata) {

        //Stop sdk
        captureSdk.stop();
        // Preserve callback
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "Mcid sdk stop.");
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);

        String frontImg = "";
		String backImg = "";

        //format result image to base64string
		if(frontImage != null)
        frontImg = android.util.Base64.encodeToString(frontImage, android.util.Base64.DEFAULT);
	
		if(backImage != null)
        backImg = android.util.Base64.encodeToString(backImage, android.util.Base64.DEFAULT);

        Map<String, String> metaData = metadata.getMetadata();

        try {
            JSONObject jsonObject = new JSONObject(metaData);
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("frontImage", frontImg);
            jsonObject2.put("backImage", backImg);
            jsonObject2.put("metaData", jsonObject);
            callbackContext.success(jsonObject2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    @Override
    public void onSuccess(CaptureResult captureResult) {
        Log.d("DOC_RES", "Success");

        //Stop sdk
        captureSdk.stop();
        // Preserve callback
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, "Mcid sdk stop.");
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);

        String frontImg = "";
		String backImg = "";

        //format result image to base64string
		if(captureResult.getSide1() != null)
        frontImg = android.util.Base64.encodeToString(captureResult.getSide1(), android.util.Base64.DEFAULT);
	
	    if(captureResult.getSide2() != null)
        backImg = android.util.Base64.encodeToString(captureResult.getSide2(), android.util.Base64.DEFAULT);

        Map<String, String> metaData = captureResult.getMetadata().getMetadata();

        try {
            JSONObject jsonObject = new JSONObject(metaData);
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("frontImage", frontImg);
            jsonObject2.put("backImage", backImg);
            jsonObject2.put("metaData", jsonObject);
            callbackContext.success(jsonObject2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    @Override
    public void onError(int i) {
        //Stop sdk
        captureSdk.stop();
        callbackContext.error("DOC Capture Failed: " + i);
        finish();
    }

    @Override
    public void onScreenChanged(CaptureScreen captureScreen) {
        // Preserve callback
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                "Mcid sdk onScreenChanged");
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);
    }

    @Override
    public void onStop(){
        super.onStop();

        edgeDetectionTimeout = null;
        autoSnapshot = null;
        autoCropping = null;
        uIElementsForStep = null;
        QACheckResultTimeout = null;
        SecondPageTimeout = null;
        detectionZone = null;
        newDoc = null;

    }

    @Override
    public void onDetectionWarnings(EnumSet<CaptureSDK.DetectionWarning> warnings) {
        if (warnings.contains(CaptureSDK.DetectionWarning.None)) {
            mQaLabels.post(new Runnable() {
                @Override
                public void run() {
                    if (mQaLabels != null) mQaLabels.setVisibility(View.INVISIBLE);
                }
            });
        } else {
            mQaLabels.post(new Runnable() {
                @Override
                public void run() {
                    if (mQaLabels != null) mQaLabels.setVisibility(View.VISIBLE);
                }
            });
            if (warnings.contains(CaptureSDK.DetectionWarning.FitDocument))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_fit_document));
                    }
                });

            if (warnings.contains(CaptureSDK.DetectionWarning.LowContrast))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_add_contrast));
                    }
                });

            if (warnings.contains(CaptureSDK.DetectionWarning.Hotspot))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_hotspot_detected));
                    }
                });

            if (warnings.contains(CaptureSDK.DetectionWarning.LowLight))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_add_light));
                    }
                });

            if (warnings.contains(CaptureSDK.DetectionWarning.Blur))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_blur_detected));
                    }
                });

            if (warnings.contains(CaptureSDK.DetectionWarning.FocusInProgress))
                mQaLabels.post(new Runnable() {
                    @Override
                    public void run() {
                        mQaLabels.setText(getString(R.string.mcid_warning_focus_in_progress));
                    }
                });
        }
    }
}
