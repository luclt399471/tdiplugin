//
//  DocViewController.h
//  SampleApp
//
//  Created by Tint Lwin Lwin Win on 4/4/19.
//

#ifndef DocViewController_h
#define DocViewController_h


#endif /* DocViewController_h */

#import <UIKit/UIKit.h>
#import <IDV_Doc/CaptureInterface.h>
#import <Cordova/CDVPlugin.h>

@interface DocViewController : UIViewController <CaptureDelegate>
@property (nonatomic, strong) NSString* callbackId;
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
@property NSNumber* detectZone1;
@property NSNumber* detectZone2;
@property NSNumber* edgeDetectionTimeout;
@property NSNumber* autoSnapshot;
@property NSNumber* autoCropping;
@property NSNumber* uIElementsForStep;
@property NSNumber* QACheckResultTimeout;
@property NSNumber* SecondPageTimeout;
@property NSNumber* captureDoc;
@property NSNumber* width;
@property NSNumber* height;
@property NSNumber* pages;
//@property (strong, nonatomic) NSNumber* newDocWidth;
//@property (strong, nonatomic) NSNumber* newDocHeight;
//@property (strong, nonatomic) NSNumber* newDocPages;
@end
