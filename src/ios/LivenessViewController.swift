//
//  LivenessViewController.swift
//  TDI
//
//  Created by Rajeshkumar on 14/11/19.
//  Copyright © 2019 Rajesh. All rights reserved.
//

import Foundation
import FaceLiveness
import main

protocol AutocaptureOnDeviceDelegate: class {
    
    // TODO: Handle ability to stop if cannot autocapture at all...
    
    // Propogate captured image
    func autocaptureOnDeviceImage(
        image: UIImage?)
}


@objc class LivenessViewController : UIViewController {
//    var command = Command.capture
//    var serverCommand = Command.capture
    var captureTimeout: Int = 0
    var selectedWorkflow: String = ""
    
    var performConstruction: Bool = false
    var captureOnDevice: Bool = true
    var loadFailureMessage: String = ""
    var isDidLoadFailure: Bool = false
    var autocapturedImage: UIImage?
    var isWorkflowRunning: Bool = false
    weak var captorCallback : CaptorCallback!
    
    @objc var WORKFLOW = "Charlie4"
    @objc var username = ""
    @objc var bgColor = ""
    @objc var fbBgColor = ""
    @objc var txtColor = ""
    @objc var txtFamily = ""
    @objc var txtStyle: NSInteger = 20
    
    var bgUIColor = UIColor.gray
    
    
    let lightBlueA5 = UIColor(red: 0.0, green: 128/255.0, blue: 255/255.0, alpha: 0.5)
    let lightBlue = UIColor(red: 0.0, green: 128/255.0, blue: 255/255.0, alpha: 1.0)

    
    @IBOutlet weak var positionDeviceLbl: UILabel!
//    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    @IBOutlet weak var devicePositionCtrl: DevicePositionControl!
    @IBOutlet weak var startStopBtn: UIButton!
    @IBOutlet weak var fLiveness: FaceLiveness!
    
//    @property (nonatomic, strong) NSString* callbackId;
//    @property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
    
    var shapeMaskLayer: CAShapeLayer?
    
    weak var autocaptureDelegate: AutocaptureOnDeviceDelegate?
    
//    public struct GlobalVariable{
//        static var callbackId = String()
//        static var commandDelegate: CDVCommandDelegate!
//    }
    
    @objc var callbackId: String = ""
    @objc var commandDelegate: CDVCommandDelegate!
    
//    public var callbackId = String()
//    public var commandDelegate: CDVCommandDelegate!
    
    override func viewDidLoad() {
//        Thread.sleep(until: '')
        self.setStaticSettings(message: "mobile")
        setupStartStopButton()
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    //
    // MARK: Conform to StaticSettingsDelegate
    //
    func setStaticSettings(message faceModel: String) {
        DispatchQueue.main.async {
            do {
                try FaceLiveness.setStaticProperty(name: StaticPropertyTag.faceModel, value: faceModel)
//                dPrint("set face model success: " + faceModel)
            }catch let err{
//                dPrint("set face model fail: \(err)")
            }
        }
    }
    
    var timer: Timer?
    
    func startTimer() {
//        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 0.1 , target: self, selector: #selector(test), userInfo: nil, repeats: false)
    }
    
    func stopTimer() {
        guard timer != nil else { return }
        timer?.invalidate()
        timer = nil
    }
    
    @objc func test() {
        do {
//            dPrint("[LAUICSVC | viewDidAppear] BEFORE isWorkflowRunning == \(self.isWorkflowRunning)")
            try self.locateAppUI()
            self.view.layoutSubviews()
        } catch FaceLivenessError.regionOfInterestNotAvailable {
//            dPrint("region of interest not available")
        } catch {
//            dPrint("Exhaustive catch on locateAppUI")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        callAwareFaceLiveness()
//        startTimer()
    }
    
    func getUIColorFromHexString(hexString: String, alpha: CGFloat = 1.0) -> UIColor {
            let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let scanner = Scanner(string: hexString)
            if (hexString.hasPrefix("#")) {
                scanner.scanLocation = 1
            }
            var color: UInt32 = 0
            scanner.scanHexInt32(&color)
            let mask = 0x000000FF
            let r = Int(color >> 16) & mask
            let g = Int(color >> 8) & mask
            let b = Int(color) & mask
            let red   = CGFloat(r) / 255.0
            let green = CGFloat(g) / 255.0
            let blue  = CGFloat(b) / 255.0
            return UIColor.init(red:red, green:green, blue:blue, alpha:alpha)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.setNeedsDisplay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(!self.txtColor.isEmpty){
            DispatchQueue.main.async {
                self.feedbackLbl.textColor = self.getUIColorFromHexString(hexString: self.txtColor)
                self.positionDeviceLbl.textColor = self.getUIColorFromHexString(hexString: self.txtColor)
            }
               }
               
               if(!self.bgColor.isEmpty){
                   bgUIColor = self.getUIColorFromHexString(hexString: self.bgColor)
               }
               
                if(!self.txtFamily.isEmpty){
                    DispatchQueue.main.async {
                        self.feedbackLbl.font = UIFont(name: self.txtFamily, size: CGFloat(self.txtStyle))
                        self.positionDeviceLbl.font = UIFont(name: self.txtFamily, size: CGFloat(self.txtStyle))
                    }
               }
               
                if(!self.fbBgColor.isEmpty){
                    DispatchQueue.main.async {
                        self.feedbackLbl.backgroundColor = self.getUIColorFromHexString(hexString: self.fbBgColor)
                        self.positionDeviceLbl.backgroundColor = self.getUIColorFromHexString(hexString: self.fbBgColor)
                    }
               }
        
        self.navigationController?.navigationBar.isHidden = true
        callAwareFaceLiveness()
        startTimer()
        
        
//        startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        LoadingOverlay.hideHourglass()
//        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    //
    // MARK: Show Overlay
    //
    
    func showHourglass(message: String) -> Void {
        DispatchQueue.main.async {
//            LoadingOverlay.showHourglass(view: self.view, message: message)
//            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
    }
    
    func hideHourglass() -> Void {
        DispatchQueue.main.async {
//            LoadingOverlay.hideHourglass()
//            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    //
    // MARK: Methods
    //
    func callAwareFaceLiveness() {
//        dPrint("callAwareFaceLiveness")
        if shapeMaskLayer != nil {
            shapeMaskLayer?.removeFromSuperlayer()
            shapeMaskLayer = nil
        }
        
        if fLiveness == nil {
//            dPrint("faceLiveness is nil")
        } else {
            do {
                fLiveness.setDevicePositionCallback(callback: didReceiveDevicePosition)
                fLiveness.setFeedbackCallback(callback: didReceiveFeedback)
                fLiveness.setWorkflowStateCallback(callback: didReceiveWorkflowState)
                // Control the max amount of time a user can attempt to capture a subject.
                // Default value of 0 means no timeout.
                try fLiveness.setProperty(name: .captureOnDevice, value: self.captureOnDevice)
                try fLiveness.setProperty(name: .constructImage, value: self.performConstruction)
                try fLiveness.setProperty(name: .username, value: self.username)
                try fLiveness.setProperty(name: .captureTimeout, value: Double(self.captureTimeout))
//                try faceLiveness.setProperty(name: .command, value: command)
                // Select Workflow
                try fLiveness.selectWorkflow(workflow: WORKFLOW, overrideParametersJson: "")
                showAppLevelUI()
                
                try fLiveness.start()
                isWorkflowRunning = true
                
            } catch FaceLivenessError.faceModelNotSupportWorkflow {
                loadFailureMessage = FaceLivenessError.faceModelNotSupportWorkflow.description + ".\nGo to Setting screen to change."
                self.isDidLoadFailure = true
                return
            } catch let err {
                let message = "EXCEPTION: setup - error " + err.localizedDescription
//                dPrint(message)
                loadFailureMessage = ""
                self.isDidLoadFailure = true
                return
            }
        }
        
    }
    
    @objc func locateAppUI() throws -> Void {
//        dPrint("locateAppUI")
        // Get the roi data from the lc
        let roidata = try fLiveness.getRegionOfInterest()
//        dPrint(roidata)
        let roiX = Double(roidata[0])
        let roiY = Double(roidata[1])
        let roiW = Double(roidata[2])
        let roiH = Double(roidata[3])
        
        let maskFrame = CGRect(
            x: 0.0,
            y: 0.0,
            width: UIScreen.main.bounds.width,
            height: UIScreen.main.bounds.height)
        
        let viewFrame = CGRect(
            x: self.fLiveness.frame.minX,
            y: self.fLiveness.frame.minY,
            width: self.fLiveness.frame.width,
            height: self.fLiveness.frame.height
        )
        
        let roiFrame = CGRect(
            x: roiX,
            y: roiY,
            width: roiW,
            height: roiH)
        
        // Set shape mask layer
        shapeMaskLayer = CAShapeLayer()
        shapeMaskLayer?.frame = maskFrame
        
        let maskFillColor = bgUIColor.withAlphaComponent(0.75).cgColor
        let maskStrokeColor = UIColor.red.cgColor
        
        drawBezierShapeMaskLayer(
            boundingViewBox: viewFrame,
            boundingBox: roiFrame,
            fillColor: maskFillColor,
            strokeColor: maskStrokeColor)
        
//        self.view.layer.addSublayer(shapeMaskLayer!)
        self.view.layer.insertSublayer(shapeMaskLayer!, above:self.view.layer)
        self.view.setNeedsDisplay()
        
        // Force display of App level UI views to front
//        self.view.bringSubviewToFront(statusLbl)
        self.view.bringSubviewToFront(feedbackLbl)
        self.view.bringSubviewToFront(startStopBtn)
        self.view.bringSubviewToFront(devicePositionCtrl)
        
        // Set button properties
      
    }
    
    func setupStartStopButton() {
        
        startStopBtn.setTitle("Back", for: UIControl.State.normal)
        startStopBtn.setTitleColor(UIColor.black, for: .normal)
        startStopBtn.titleLabel?.textAlignment = .center
        startStopBtn.backgroundColor = UIColor.white
        startStopBtn.layer.cornerRadius = 5
        startStopBtn.layer.borderWidth = 1
        startStopBtn.layer.borderColor = UIColor.black.cgColor
//
        // Associate action with button
        // Can do this through Storyboard/designer, or
        // programmatically. Shown here programmatically
        
        startStopBtn.isEnabled = true
    }
    
    private func drawBezierShapeMaskLayer(
        boundingViewBox: CGRect,
        boundingBox: CGRect,
        fillColor: CGColor,
        strokeColor: CGColor)
    {
        // Create path and add bounding box
        let viewPath = UIBezierPath()
        viewPath.append(UIBezierPath(rect: boundingViewBox))
        
        // Add silhouette path
        viewPath.append(createSilhouetteBezierPath(boundingBox: boundingBox, verticalParts: 3))
        
        // Set shape mask
        shapeMaskLayer?.path = viewPath.cgPath
        shapeMaskLayer?.fillRule = CAShapeLayerFillRule.evenOdd
        shapeMaskLayer?.fillColor = fillColor
        shapeMaskLayer?.strokeColor = strokeColor
        shapeMaskLayer?.lineWidth = 2
        shapeMaskLayer?.opacity = 0.8
    }
    
    func createSilhouetteBezierPath(boundingBox: CGRect, verticalParts: CGFloat) -> UIBezierPath {
        let boxHeight = boundingBox.height
        let boxWidth = boundingBox.width
        let boxMinX = boundingBox.minX
        let boxMinY = boundingBox.minY
        let boxMaxX = boundingBox.maxX
        let boxMaxY = boundingBox.maxY
        let quarterWidth = boxWidth / 4
        let halfWidth = boxWidth / 2
        let verticalPartHeight = boxHeight / verticalParts
        let verticalPartHalfHeight = verticalPartHeight / 2
        
        // Start path
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: boxMinX, y: boxMinY + verticalPartHeight))
        
        // First half of top
        bezierPath.addCurve(
            to: CGPoint(x: boxMinX + halfWidth, y: boxMinY),
            controlPoint1: CGPoint(x: boxMinX, y: boxMinY + verticalPartHalfHeight),
            controlPoint2: CGPoint(x: boxMinX + quarterWidth, y: boxMinY)
        )
        
        // Second half of top
        bezierPath.addCurve(
            to: CGPoint(x: boxMaxX, y: boxMinY + verticalPartHeight),
            controlPoint1: CGPoint(x: boxMaxX - quarterWidth, y: boxMinY),
            controlPoint2: CGPoint(x: boxMaxX, y: boxMinY + verticalPartHalfHeight)
        )
        
        // Connect top to bottom
        bezierPath.addLine(
            to: CGPoint(x: boxMaxX, y: boxMaxY - verticalPartHeight)
        )
        
        // First half of bottom
        bezierPath.addCurve(
            to: CGPoint(x: boxMinX + halfWidth, y: boxMaxY),
            controlPoint1: CGPoint(x: boxMaxX, y: boxMaxY - verticalPartHalfHeight),
            controlPoint2: CGPoint(x: boxMaxX - quarterWidth, y: boxMaxY)
        )
        
        // Second half of bottom
        bezierPath.addCurve(
            to: CGPoint(x: boxMinX, y: boxMaxY - verticalPartHeight),
            controlPoint1: CGPoint(x: boxMinX + quarterWidth, y: boxMaxY),
            controlPoint2: CGPoint(x: boxMinX, y: boxMaxY - verticalPartHalfHeight)
        )
        
        // Connect bottom to top
        bezierPath.addLine(
            to: CGPoint(x: boxMinX, y: boxMinY + verticalPartHeight)
        )
        
        return bezierPath
    }

    deinit {
        fLiveness = nil
    }
    
    private func showAppLevelUI() {
        DispatchQueue.main.async {
            
            self.positionDeviceLbl.isHidden = false
//            self.statusLbl.isHidden = false
            self.feedbackLbl.isHidden = true
            self.devicePositionCtrl.isHidden = false
            self.startStopBtn.isHidden = false
            self.shapeMaskLayer?.isHidden = false
            self.shapeMaskLayer?.strokeColor = UIColor.red.cgColor
            self.shapeMaskLayer?.fillColor = self.bgUIColor.withAlphaComponent(0.75).cgColor
        }
    }
    
    // Handler for device position
    func didReceiveDevicePosition(y: CGFloat, isInPosition: Bool) {
        
        // Update devicePositionControl
        self.devicePositionCtrl.updateSegments(y: y, isInPosition: isInPosition)
    }
    
    // Handler for feedback result
    func didReceiveFeedback(feedback: FeedbackResult) -> Void {
        
        DispatchQueue.main.async {
            self.feedbackLbl.text = feedback.autocaptureFeedback.description
//            print(feedback.autocaptureFeedback.description)
        }
    }
    
    // Handler for workflow state
    private func didReceiveWorkflowState(workflowState: WorkflowState, data: String) -> Void {
        
        switch(workflowState) {
        case .workflowPreparing:
            DispatchQueue.main.async {
                
                // Make sure component is not hidden
                self.fLiveness.isHidden = false
                
                // Content
//                self.statusLbl.text = NSLocalizedString("Position Device", comment: "")
                
                // Show all UI
                self.showAppLevelUI()
                self.positionDeviceLbl.isHidden = false
                self.feedbackLbl.isHidden = true
                
                self.positionDeviceLbl.text = NSLocalizedString("Position Device Vertically", comment: "")
 
            }
            break
        case .workflowDeviceInPosition:
            DispatchQueue.main.async {
//                self.statusLbl.text = NSLocalizedString("Position Face", comment: "")
                self.positionDeviceLbl.isHidden = true
                self.feedbackLbl.isHidden = false
            }
            break
        case .workflowHoldSteady:
            DispatchQueue.main.async {
                
                self.feedbackLbl.isHidden = false
                self.feedbackLbl.text = NSLocalizedString("Hold", comment: "")
                
                self.shapeMaskLayer?.strokeColor = self.bgUIColor.cgColor
                self.shapeMaskLayer?.fillColor = self.lightBlueA5.cgColor
            }
            break
        case .workflowCapturing:
            DispatchQueue.main.async {
//                self.statusLbl.text = NSLocalizedString("Hold", comment: "")
                self.feedbackLbl.isHidden = true
                self.shapeMaskLayer?.strokeColor = UIColor.green.cgColor
                self.shapeMaskLayer?.fillColor = self.lightBlue.cgColor
            }
            break
        case .workflowEvent:
            DispatchQueue.main.async {
                
//                self.statusLbl.text = NSLocalizedString(data, comment: "")
                self.feedbackLbl.isHidden = false
            }
            break
        case .workflowShowUI:
            DispatchQueue.main.async {
                // Show all UI
//                self.statusLbl.isHidden = false
                self.feedbackLbl.isHidden = false
                self.devicePositionCtrl.isHidden = false
//                self.startStopButton.isHidden = false
                self.shapeMaskLayer?.isHidden = false
            }
            break
        case .workflowHideUI:
            DispatchQueue.main.async {
                // Hide all UI
//                self.statusLbl.isHidden = true
                self.feedbackLbl.isHidden = true
                self.devicePositionCtrl.isHidden = true
//                self.startStopButton.isHidden = true
                self.shapeMaskLayer?.isHidden = true
            }
            break
        case .workflowPostProcessing:
            DispatchQueue.main.async {
                
//                LoadingOverlay.showHourglass(view: self.view, message: "...Calculating...")
//                MBProgressHUD.showAdded(to: self.view, animated: true)
                
                // Hide all UI
//                self.statusLbl.isHidden = true
                self.feedbackLbl.isHidden = true
                self.devicePositionCtrl.isHidden = true
             //   self.startStopButton.isHidden = true
                self.shapeMaskLayer?.isHidden = true
            }
            break
        case .workflowComplete:
            
            DispatchQueue.global(qos: .background).async {
                // These helper hourglass functions put onto main thread
                DispatchQueue.main.sync {
                    self.hideHourglass()
                    self.showHourglass(message: "Loading Data...")
                }
                
                self.isWorkflowRunning = false
                
                //
                // MARK: Section for Autocapture on device
                //
//                if self.captureOnDevice && self.serverCommand == Command.capture {
                 if self.captureOnDevice {
                    DispatchQueue.main.sync {
                        do {
                            self.autocapturedImage = try self.fLiveness.getCapturedImage()
                        } catch let err {
                            self.dPrint("getCapturedImage threw - no image: \(err)")
                        }
                    }
                    self.notifyAutocaptureDidOccur(image: self.autocapturedImage)
                }
                
                //
                // MARK: Section for populating server package
                //
                DispatchQueue.main.async {
                    do {
                        let serverPackage =  try self.fLiveness.getServerPackage()
                        
                        /*
                        let data : Data = NSKeyedArchiver.archivedData(withRootObject: serverPackage)
                        #warning("Remove below line")
                        dPrint("Package Size\(data.count)")
                        */
                        
                        let capturedImage = try self.fLiveness.getCapturedImage()
                        /*
                        #warning("Remove below line")
                        let imageData = capturedImage.jpegData(compressionQuality: 1)?.count
                        print("Image data \(imageData)")
                         */
                        self.livenessSuccessFullCapture(serverPackage: serverPackage, capturedImage: capturedImage)
                        
                    } catch {
                        self.dPrint("test getServerPackage or sendRequestToServer threw")
                        
                        self.isDidLoadFailure = true
                        DispatchQueue.main.sync {
//                            LoadingOverlay.hideHourglass()
//                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        self.didReceiveWorkflowState(workflowState: WorkflowState.workflowAborted,data: "")
                    }
                }
                
            }
            
            break
            
        case .workflowAborted:
            isWorkflowRunning = false
            
            // Hide UI
            DispatchQueue.main.async {
                // Hide all UI
//                self.statusLbl.isHidden = true
                
                self.feedbackLbl.isHidden = true
                self.devicePositionCtrl.isHidden = true
            //    self.startStopButton.isHidden = true
                self.shapeMaskLayer?.isHidden = true
                
                // Segue back to Home VC where handler will throw up an alert message
                // from there displaying timedout abort occured.
//                self.performSegue(withIdentifier: "timedOutUnwindToHome", sender: self)
            }
            
            break
        case .workflowTimedOut:
            break
        }
    }
    
    func notifyAutocaptureDidOccur(image: UIImage?) {
        
        dPrint("[LAUICSVC | notifyAutocaptureDidOccur] thread: \(Thread.current)")
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
//            if strongSelf.captureOnDevice && strongSelf.serverCommand == Command.capture {
            if strongSelf.captureOnDevice {
                strongSelf.autocaptureDelegate?.autocaptureOnDeviceImage(image: image)
                strongSelf.dismiss(animated: true, completion: {
                })
            }
        }
    }
    
    @IBAction func onStopButtonClick() {
        showErrorMessage(message: "Do you want to stop capture?", viewController: self)
    }
    
    func showErrorMessage(message : String,viewController : UIViewController) {
        let alertController = UIAlertController.init(title: "Cancel", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: { (action) in
            
        }))
        alertController.addAction(UIAlertAction.init(title: "Stop", style: .default, handler: { (action) in
            do {
                try self.fLiveness.stop()
            } catch {
                let message = "EXCEPTION from faceLiveness.start()"
                self.isDidLoadFailure = true
                self.dPrint(message)
            }
            self.livenessCaptureFailed()
        }))
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func livenessSuccessFullCapture(serverPackage : [String : Any], capturedImage : UIImage) {
        
        let jsonData: NSData
        
        do {
            
            do {
                try fLiveness.stop()
            } catch {
                let message = "EXCEPTION from faceLiveness.start()"
                self.isDidLoadFailure = true
                dPrint(message)
            }
            
            jsonData = try JSONSerialization.data(withJSONObject: serverPackage, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            
            guard let serverPackageData = jsonString.data(using: .utf8)?.base64EncodedString() else {
                livenessCaptureFailed()
                return
            }
            guard let imageData = capturedImage.jpegData(compressionQuality: 1)?.base64EncodedString() else {
                livenessCaptureFailed()
                return
            }
            
            
            var myDict:NSDictionary = ["faceImage" : imageData, "metaData":serverPackageData]
            
            let pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: (myDict as! [AnyHashable : Any])
            )
            
            self.commandDelegate.send(pluginResult, callbackId:callbackId)
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            
            
        } catch _ {
            dPrint ("JSON Failure")
        }
    }
    
    func livenessCaptureFailed() {
        
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: "Face Capture Failed"
        )
        
        self.commandDelegate.send(pluginResult, callbackId:callbackId)
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @inline(__always)
    func dPrint(_ object: Any) {
        #if DEBUG
        Swift.print(object)
        #endif
    }
}

