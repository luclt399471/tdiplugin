//
//  DocViewController.m
//  SampleApp
//
//  Created by Tint Lwin Lwin Win on 4/4/19.
//

#import <Foundation/Foundation.h>
#   import "DocViewController.h"
#import "Store.h"
#import "DeviceOrientationSingleton.h"

@interface DocViewController()
@property (strong, nonatomic) IBOutlet CaptureInterface *camera;
@property (weak, nonatomic) IBOutlet UILabel *detectWarning;
@property (weak, nonatomic) IBOutlet UIButton *cancelCaptureBtn;
@end

@implementation DocViewController

-(void)viewDidLoad{
    [super viewDidLoad];
        self.camera.hidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    _camera = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // start the doc capture
//    self.resultView.hidden = true;
    
    if(self.width != nil){
        Document* document = [[Document alloc] initWithWidth:[self.width floatValue] andHeight:[self.height floatValue] andNumPages:[self.pages intValue]];
        NSArray *list = [NSArray arrayWithObject:document];
        [self.camera setCaptureDocuments:list];
    }
    
    if(self.captureDoc != nil){
        switch ([self.captureDoc intValue]) {
            case 0: [self.camera setCaptureDocuments:DocumentModeIdDocument]; break;
            case 1: [self.camera setCaptureDocuments:DocumentModePassport]; break;
            case 2: [self.camera setCaptureDocuments:DocumentModeICAO]; break;
            default: [self.camera setCaptureDocuments:DocumentModeIdDocument]; break;
        }
    }
    
    if(self.detectZone2 != nil && self.detectZone1 != nil){
        [self.camera setDetectionZoneSpace:[self.detectZone1 intValue] andAspectRatio:[self.detectZone2 floatValue]];}
    
    if(self.edgeDetectionTimeout != nil)
    [self.camera setEdgeDetectionTimeout:[self.edgeDetectionTimeout intValue]];
    
    if(self.autoSnapshot != nil)
    [self.camera setAutoSnapshot:[self.autoSnapshot boolValue]];
    
    if(self.autoCropping != nil)
    [self.camera setAutoCropping:[self.autoCropping boolValue]];
    
    if(self.uIElementsForStep != nil){
        switch ([self.uIElementsForStep intValue]) {
            case Detecting:
            [self.camera hideUIElementsForStep:Detecting];
            break;
            case Cropping:
            [self.camera hideUIElementsForStep:Cropping];
            break;
            case ResultOK:
            [self.camera hideUIElementsForStep:ResultOK];
            break;
            case ResultKO:
            [self.camera hideUIElementsForStep:ResultKO];
            break;
        }
    }
    
    if(self.QACheckResultTimeout != nil)
    [self.camera setQACheckResultTimeout:[self.QACheckResultTimeout intValue]];
    
    if(self.SecondPageTimeout != nil)
    [self.camera setSecondPageTimeout:[self.SecondPageTimeout intValue]];
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt: ((DeviceOrientationSingleton*)[DeviceOrientationSingleton sharedManager]).interfaceOrientation]
                                forKey:@"orientation"];
    
    [self.camera setDetectionWarningsDelegate:self];
    [self.camera initWithCompletion:^(BOOL isCompleted, int errorCode) {
        if(isCompleted) {
            double delayInSeconds = 4.0f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.camera start:self];
            });
        } else {
            NSLog(@"Error on init: %i", errorCode);
        }
    }];
}

- (void) viewWillDisappear:(BOOL)animated
{
    NSLog(@"camera stop");
    [self.camera stop];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationMaskLandscapeRight);
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) onSuccess:(CaptureResult *)captureResult
{
    // do anything after the SDK makes a succesfull capture
    
    NSString *text = @"---------\n";
    
    if (captureResult.metadata != NULL) {
        NSArray *keys = [captureResult.metadata allKeys];
        text = [text stringByAppendingString: @"Document meta data : \n"];
        for (NSString *key in keys) {
            text = [text stringByAppendingString: [NSString stringWithFormat:@"  - %@ = %@\n", key, [captureResult.metadata objectForKey:key]]];
        }
    }
    
    NSString *textMrz = @"---------\n";
    
    if (captureResult.mrz != NULL) {
        NSArray *keys = [captureResult.mrz allKeys];
        textMrz = [textMrz stringByAppendingString: @"Document mrz data : \n"];
        for (NSString *key in keys) {
            textMrz = [textMrz stringByAppendingString: [NSString stringWithFormat:@"  - %@ = %@\n", key, [captureResult.mrz objectForKey:key]]];
        }
    }
    
    NSString *side1_base64String = @"";
    NSString *side2_base64String = @"";
    
    side1_base64String = [captureResult.side1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if(captureResult.side2)
    side2_base64String = [captureResult.side2 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    id objects[] = {side1_base64String, side2_base64String, text, textMrz};
    id keys[] = {@"frontImage",@"backImage",@"metaData",@"MRZ"};
    
    NSDictionary *dicResult = [[NSDictionary alloc] initWithObjects:objects forKeys:keys count:4];
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dicResult];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }];
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - DetectionWarningDelegate
-(void) detectionWarnings:(DetectionWarning)warnings{
    dispatch_async(dispatch_get_main_queue(), ^{

        if((warnings & FocusInProgress) != 0){
            //        NSLocalized
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_focus_in_progress", nil)];
        }
        
        if((warnings & FitDocument) != 0){
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_fit_document", nil)];
        }
        
        if((warnings & Hotspot) != 0){
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_hotspot_detected", nil)];
        }
        
        if((warnings & LowContrast) != 0){
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_add_contrast", nil)];
        }
        
        if((warnings & LowLight) != 0){
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_add_light", nil)];
        }
        
        if((warnings & Blur) != 0){
            [self.detectWarning setText:NSLocalizedString(@"mcid_warning_blur", nil)];
        }
        
        if(warnings == 0){
            [self.detectWarning setText:@""];
        }
        
    });
}

#pragma mark - CaptureDelegate
- (void) onSuccess:(NSData*)p_side1 :(NSData*)p_side2 :(NSDictionary*)p_metaData
{
    // do anything after the SDK makes a succesfull capture
    NSString *text = @"---------\n";
    
    if (p_metaData != NULL) {
        NSArray *keys = [p_metaData allKeys];
        text = [text stringByAppendingString: @"Document meta data : \n"];
        for (NSString *key in keys) {
            text = [text stringByAppendingString: [NSString stringWithFormat:@"  - %@ = %@\n", key, [p_metaData objectForKey:key]]];
        }
    }
    
    NSString *side1_base64String = @"";
    NSString *side2_base64String = @"";
    side1_base64String = [p_side1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if(p_side2)
    side2_base64String = [p_side2 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    id objects[] = {side1_base64String, side2_base64String, text};
    id keys[] = {@"frontImage",@"backImage",@"metaData"};
    
    NSDictionary *dicResult = [[NSDictionary alloc] initWithObjects:objects forKeys:keys count:3];
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dicResult];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }];
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
    
}

- (void)onError:(CaptureError)p_failureCode
{
    [[Store getInstance] addValue:p_failureCode forKey:@"IDVDOC_SDK_ERROR_CODE"];
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"IDVDOC_SDK_ERROR"];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }];
    
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
    
}


- (IBAction)cancelCapture:(id)sender {
    NSLog(@"cancel capture");
    [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)onScreenChanged:(CaptureScreen)screen {
        switch(screen) {
        case InDetecting:
                NSLog(@"State: In capture screen");
                break;
        case OutDetecting:
                NSLog(@"State: Out capture screen");
                break;
        case InCropping:
                NSLog(@"State: In cropping screen");
                break;
        case OutCropping:
                NSLog(@"State: Out cropping screen");
                break;
        case InResultOK:
                NSLog(@"State: In result ok screen");
                break;
        case OutResultOK:
                NSLog(@"State: Out result ok screen");
                break;
        case InResultKO:
                NSLog(@"State: In result ko screen");
                break;
        case OutResultKO:
                NSLog(@"State: Out result ko screen");
                break;
        default:
                break;
        }
    }

@end
