#import "TdiPlugin.h"
#import "DocViewController.h"
#import "<ProjectName>-Swift.h"

@interface TdiPlugin()

@property(nonatomic, strong) DocViewController* docController;
@property(nonatomic, strong) LivenessViewController* livenessController;
@property(nonatomic, strong) TDIController* tdiController;

@end

@implementation TdiPlugin

int docChoice = 0;
int detectZone1;
float detectZone2;
int edgeDetectionTimeout;
Boolean autoSnapshot;
Boolean autoCropping;
NSString* uIElementsForStep;
int QACheckResultTimeout;
int SecondPageTimeout;

- (void)pluginInitialize {
    
    self.docController = [[DocViewController alloc] init];
    self.livenessController = [[LivenessViewController alloc] initWithNibName:@"LivenessView" bundle:nil];
    self.tdiController = [[TDIController alloc] initWithNibName:@"TDIController" bundle:nil];
}

#pragma mark public

- (void)init:(CDVInvokedUrlCommand*)command {
//    DocViewController *docController = [[DocViewController alloc] init];
    self.docController.callbackId = command.callbackId;
    self.docController.commandDelegate = self.commandDelegate;
    self.docController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.viewController presentViewController:self.docController animated:YES completion:nil];
}

- (void)setDetectionZone:(CDVInvokedUrlCommand*)command{
    self.docController.detectZone1 = [command.arguments objectAtIndex:0];
    self.docController.detectZone2 = [command.arguments objectAtIndex:1];
}
- (void)setCaptureDocuments:(CDVInvokedUrlCommand*)command{
    self.docController.captureDoc = [command.arguments objectAtIndex:0];
}
- (void)setCaptureNewDocument:(CDVInvokedUrlCommand*)command{
    self.docController.width = [command.arguments objectAtIndex:0];
    self.docController.height = [command.arguments objectAtIndex:1];
    self.docController.pages = [command.arguments objectAtIndex:2];
}
- (void)setAutoSnapshot:(CDVInvokedUrlCommand*)command{
    self.docController.autoSnapshot = [command.arguments objectAtIndex:0];
}
- (void)setEdgeDetectionTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.edgeDetectionTimeout = [command.arguments objectAtIndex:0];
}
- (void)setAutocropping:(CDVInvokedUrlCommand*)command{
    self.docController.autoCropping = [command.arguments objectAtIndex:0];
}
- (void)hideUIElementsForStep:(CDVInvokedUrlCommand*)command{
    self.docController.uIElementsForStep = [command.arguments objectAtIndex:0];
}
- (void)setQACheckResultTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.QACheckResultTimeout = [command.arguments objectAtIndex:0];
}
- (void)setSecondPageTimeout:(CDVInvokedUrlCommand*)command{
    self.docController.SecondPageTimeout = [command.arguments objectAtIndex:0];
}
- (void)initFaceCapture:(CDVInvokedUrlCommand*)command {
    self.livenessController.callbackId = command.callbackId;
    self.livenessController.commandDelegate = self.commandDelegate;
    self.livenessController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.viewController presentViewController:self.livenessController animated:YES completion:nil];
}

- (void)initTDI:(CDVInvokedUrlCommand*)command {
    self.tdiController.callbackId = command.callbackId;
    self.tdiController.commandDelegate = self.commandDelegate;
    self.tdiController.modalPresentationStyle = UIModalPresentationCustom;
    [self.viewController presentViewController:self.tdiController animated:YES completion:nil];
    
}

- (void)setTDIConfig:(CDVInvokedUrlCommand*)command{
    self.tdiController.scenarioName = [command.arguments objectAtIndex:0];
    self.tdiController.baseUrl = [command.arguments objectAtIndex:1];
    self.tdiController.token = [command.arguments objectAtIndex:2];
    self.tdiController.tennatID = [command.arguments objectAtIndex:3];
}

- (void)setLivenessConfig:(CDVInvokedUrlCommand *)command{
    self.livenessController.WORKFLOW = [command.arguments objectAtIndex:0];
    self.livenessController.username = [command.arguments objectAtIndex:1];
}

- (void)setTextColor:(CDVInvokedUrlCommand *)command{
    self.livenessController.txtColor = [command.arguments objectAtIndex:0];
}
- (void)setBackgroundColor:(CDVInvokedUrlCommand *)command{
    self.livenessController.bgColor = [command.arguments objectAtIndex:0];
}
- (void)setTextType:(CDVInvokedUrlCommand *)command{
    self.livenessController.txtFamily = [command.arguments objectAtIndex:0];
    self.livenessController.txtStyle = [[command.arguments objectAtIndex:1] intValue];
}
- (void)setFeedbackBackgroundColor:(CDVInvokedUrlCommand *)command{
    self.livenessController.fbBgColor = [command.arguments objectAtIndex:0];
}

@end
