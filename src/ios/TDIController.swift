//
//  TDIController.swift
//  SampleTestApp
//
//  Created by Rajeshkumar on 1/8/19.
//  Copyright © 2019 Rajesh. All rights reserved.
//

import UIKit
import TDI
import AVFoundation
import main
import Security
import CommonCrypto
import IDV_Face_Z

private let CLASS_NAME = "com.gemalto.tdi.internal.CaptorResultReceiver"
let ACTION_CAPTURE_DATA = "\(CLASS_NAME).ACTION_CAPTURE_DATA"
let EXTRA_IMAGE_DATA = "\(CLASS_NAME).EXTRA_IMAGE_DATA"
let EXTRA_FAILURE_DATA = "\(CLASS_NAME).EXTRA_FAILURE_DATA"

public let ResultNotification = NSNotification.Name("GetResultNotification")
let APPTOKEN = "dOixmDcrTSWxZtzXruP1ha0ZRoQEf0TT"

struct DemoServer {
    let baseUrl = "https://appdemo.tdi.ew1.msi-dev.acloud.gemalto.com"
    let scenarioName = "Demo1-99645574-62c2-47f1-9a39-9ab3498484c0"
    let tennatID = "demo"
   // let token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjAifQ.eyJhdWQiOiJUREktRGVtby1UaGFsZXMiLCJyb2xlcyI6WyJzY3M6ZXhlY3V0ZVNjZW5hcmlvIl0sImlzcyI6ImRlbW8tdGhhbGVzLXNhbGVzIiwiZXhwIjoxNTg3MDg0MTg4LCJqdGkiOiJhYmU1MThlZS1mMGRiLTExZTktYTkzNS0wMDUwNTZiZGEzMWEifQ.UirtX4OwznjNsgNdw032BdLtX65ej3hJ8zKOGCSJVs7nJVmpCVVw2B3kAw3yeCWrAigy2DzMzUYtBA5p4ukluw"
    let token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IjAifQ.eyJhdWQiOiJUREktRGVtby1UaGFsZXMiLCJyb2xlcyI6WyJzY3M6ZXhlY3V0ZVNjZW5hcmlvIl0sImlzcyI6ImRlbW8tdGhhbGVzLXNhbGVzIiwiZXhwIjoxNTg5ODM0ODI3LCJqdGkiOiIwMDYxMTU5MC0wOWUwLTExZWEtOWQwZS0wMDUwNTZiZGEzMWEifQ.pzg0n9rarv3p5fPtZct0hDGsFLvcn6ciCvF-au4oC1IbELGrS34NEypfUvtyLoqZput4iSIacT07UEpn2WQSKg"
}

struct StagingServer {
    let baseUrl = "https://app.tdi.ew1.msi-stg.acloud.gemalto.com"
    let scenarioName = "Starhub-FaceTec-Kyc"
//    let scenarioName = "Spitfire-liveness-362ff854-0839-2453-14a7-958acb8650d1"
//    let tennatID = "demo"
    let tennatID = "starhub"
    let token = "Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRpMWtpZDEifQ.eyJhdWQiOiJURElEZW1vQ2xpZW50Iiwicm9sZXMiOlsic2NzOmV4ZWN1dGVTY2VuYXJpbyJdLCJpc3MiOiJEZW1vSXNzdWVyMUZvclRlc3QiLCJleHAiOjE1OTg2NzI1NzEsImp0aSI6ImMzZTBlMDIwLTA3ZDQtNDNhMi1iMjFmLTY0MDFhY2I5ZTNhMiJ9.U_7baFvnN9qRojvjuzcUErh3d-g4tENPd8RrqnVyURaaJyME4K0pZOKfal8PXLDWzBqflKpWcMIn60euYTm6-g"
}

struct AwareServer {
    let baseUrl = "https://appha.tdi.ew1.msi-dev.acloud.gemalto.com"
    let scenarioName = "Mobile-Liveness-Aware-Only-8cc38d51"
    let tennatID = "starhub"
    
    let token = "Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRpMWtpZDEifQ.eyJhdWQiOiJURElEZW1vQ2xpZW50Iiwicm9sZXMiOlsic2NzOmV4ZWN1dGVTY2VuYXJpbyJdLCJpc3MiOiJEZW1vSXNzdWVyMUZvclRlc3QiLCJleHAiOjE1OTg2NzI1NzEsImp0aSI6ImMzZTBlMDIwLTA3ZDQtNDNhMi1iMjFmLTY0MDFhY2I5ZTNhMiJ9.U_7baFvnN9qRojvjuzcUErh3d-g4tENPd8RrqnVyURaaJyME4K0pZOKfal8PXLDWzBqflKpWcMIn60euYTm6-g"
}

//let BASE_URL = "https://appdemo.tdi.ew1.msi-dev.acloud.gemalto.com"

let CATEGORY_SELFIE = "Selfie"
let CATEGORY_DOCUMENT = "ID Card"
let CATEGORY_PASSPORT = "Passport"
let CATEGORY_LIVENESS = "AwareLiveness"
let CATEGORY_A4DOCUMENT = "A4"

//let SCENARIO_NAME = "Starhub-FaceTec-Microblink-Linear"
//let SCENARIO_NAME = "Starhub-FaceTec-Kyc"

// 21 Oct
// New Demo Credential
/*
 let BASE_URL = "https://app.tdi.ew1.msi-stg.acloud.gemalto.com"
 let SCENARIO_NAME = "Spitfire-liveness-12edfce6-a83d-11e9-ba9f-06c966f6e8d8"
 let token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6ImRpMWtpZDEifQ.eyJhdWQiOiJURElEZW1vQ2xpZW50Iiwicm9sZXMiOlsic2NzOmV4ZWN1dGVTY2VuYXJpbyJdLCJpc3MiOiJEZW1vSXNzdWVyMUZvclRlc3QiLCJleHAiOjE1NzE4ODk3NjAsImp0aSI6ImY2NjZlMjYwLWYwOTItMTFlOS1iMmJhLTU0OGE0NzA2YTMxMCJ9.qpYrBEnXaA5DPnInbMe0qnz0wySnCosHbn8O5OGXz-eWOmJhwDLQZ8g-NvhxXYiz2Esploth8W47j_zJt-C2ww"
 let TENANT_ID = "starhub"

*/
@objc class TDIController: UIViewController {
    
    var tdiMobileSdk : TdiMobileSdk!
    private var session: Session? = nil
    var tasks = NSMutableArray()
    
    @objc var callbackId: String = ""
    @objc var commandDelegate: CDVCommandDelegate!
    
    @objc var baseUrl: String = ""
    @objc var scenarioName : String = ""
    @objc var tennatID : String = ""
    @objc var token : String = ""
    
    private var txtCode: UITextView! = UITextView()
    private var txtMessage: UITextView! = UITextView()
    private var txtName: UITextView! = UITextView()
    private var txtDocVerif: UITextView! = UITextView()
    private var txtFaceVerif: UITextView! = UITextView()
    private var txtLivelinessVerif: UITextView! = UITextView()
    
    var isAwareTesting = false
    private var observer: AnyObject?
    
    #warning("Change Here which server you want to use")
    let server = DemoServer.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tdiMobileSdk = TdiMobileSdk.shared
        tdiMobileSdk.setViewController(myViewController: self)
        tdiMobileSdk.delegate = self        
//        checkFaceTechLicence()        
 
      //  testMyCaptor()
    }
    
//    func testMyCaptor() {
//        tdiMobileSdk = TdiMobileSdk.shared
//        tdiMobileSdk.setViewController(myViewController: self)
//        tdiMobileSdk.delegate = self
//        tdiMobileSdk.captorProvider.set(name: "MyCaptor", captor: MyCaptor.init())
//        
//        session?.resume(taskId: "waitForFace")
//    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  self.checkCamera()
        
        subscribe()
//        checkFaceTechLicence()
    }
    
    func checkFaceTechLicence() {
        let license = self.getLicense(fileName:"idv_face-z.license")
        
        FaceCaptureSDK.initialize(withLicenceText: license, appToken: APPTOKEN) { (success, messageString) in
            if success {
                Swift.print("License is valid.")
            } else {
                Swift.print(messageString!)
            }
        }
    }
    
    func getLicense(fileName : NSString) -> String {
        if let path : String = Bundle.main.path(forResource: fileName.deletingPathExtension, ofType: fileName.pathExtension){
            Swift.print("Path : \(path)")
            var licence = ""
            do {
                licence = try String.init(contentsOfFile: path, encoding: String.Encoding.utf8)
            } catch {
                Swift.print("Error")
            }
            return licence
        } else {
            Swift.print("path not found")
            return ""
        }
    }
    
    func subscribe() {
        guard observer == nil else { return }
        observer = NotificationCenter.default.addObserver(forName: ResultNotification, object: nil, queue: nil) {[weak self] (notification) in
            guard let `self` = self else {
                return
            }
            self.onResultButtonClick()
        }
    }
    
    func unsubscribe() {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
            self.observer = nil
        }
    }
    
    deinit {
        unsubscribe()
    }

    override func viewWillDisappear(_ animated: Bool) {
        unsubscribe()
    }
    
    @IBAction func onVerificationButtonClicked() {
//        self.callApi()
//        return
        isAwareTesting = false
        
        if session != nil {
            if session?.status != Status.completed {
                Toast(text: "Multiple session creation not allowed. Either stop previous session or continue using previous session.",delay: Delay.short, duration: Delay.long).show()
                return
            }
        }
        
        session = tdiMobileSdk.newSession(tdiServerURL: baseUrl, scenarioName: scenarioName, jwtToken: token, tenantId:server.tennatID, correlationId: tennatID)
        if session == nil {
            Toast(text: "Failed to initialize session:",delay: Delay.short, duration: Delay.long).show()
        } else {
            self.session?.start()
            Toast(text: "Initialializing session:",delay: Delay.short, duration: Delay.long).show()
        }
        
        if ConnectionCheck.isConnectedToNetwork()
        {
            Swift.print("Connected")
        }
        else{
            Toast(text: "Please check your internet connection.",delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    func onAwareVerificationButtonClicked() {
        //        self.callApi()
        //        return
        let awareServer = AwareServer.init()
        session = tdiMobileSdk.newSession(tdiServerURL: awareServer.baseUrl, scenarioName: awareServer.scenarioName, jwtToken: awareServer.token, tenantId:awareServer.tennatID, correlationId: awareServer.tennatID)
        if session == nil {
            Toast(text: "Failed to initialize session:",delay: Delay.short, duration: Delay.long).show()
        } else {
            self.session?.start()
            isAwareTesting = true
            Toast(text: "Initialializing session:",delay: Delay.short, duration: Delay.long).show()
        }
        
        if ConnectionCheck.isConnectedToNetwork()
        {
            Swift.print("Connected")
            //Online related Business logic
        }
        else{
            Toast(text: "Please check your internet connection.",delay: Delay.short, duration: Delay.long).show()
            // offline related business logic
        }
    }
    
    @IBAction func onStopSessionButtonClick() {
        guard let tempSession = session else {
            Toast(text: "Session is not initialized.", delay: Delay.short, duration: Delay.long).show()
            return
        }
        self.tasks.removeAllObjects()
        tempSession.stop()
        Toast(text: "Session have been stopped", delay: Delay.short, duration: Delay.long).show()
    }
    
    @IBAction func callPassportScan() {
        if session != nil {
            let category = self.tasks.filtered(using: NSPredicate.init(format: "category like %@", CATEGORY_PASSPORT))
            Swift.print(category)
            if category.count >= 1 {
                self.session?.resume(taskId: "waitForPassport")
            } else {
                Toast.init(text: "Task is not available. ", delay: Delay.short, duration: Delay.long).show()
            }
        } else {
            Toast.init(text: "Session is not initialized. ", delay: Delay.short, duration: Delay.long).show()
        }
//        let captorPassport : Captor? = tdiMobileSdk.captorProvider.get(name: "Passport")
//        captorPassport?.execute(callback: self)
    }
    
    @IBAction func onIdDocumentButtonClick() {
        if session != nil {
            let category = self.tasks.filtered(using: NSPredicate.init(format: "category like %@", CATEGORY_DOCUMENT))
            Swift.print(category)
            if category.count >= 1 {
                self.session?.resume(taskId: "waitForId")
            } else {
                 Toast.init(text: "Task is not available. ", delay: Delay.short, duration: Delay.long).show()
            }
        } else {
            Toast.init(text: "Session is not initialized. ", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    @IBAction func onA4DocumentButtonClick() {
        let captorA4Document : Captor? = tdiMobileSdk.captorProvider.get(name: CATEGORY_A4DOCUMENT)
        captorA4Document?.execute(callback: self)

        /*
        if session != nil {
            let category = self.tasks.filtered(using: NSPredicate.init(format: "category like %@", CATEGORY_A4DOCUMENT))
            Swift.print(category)
            if category.count >= 1 {
                self.session?.resume(taskId: "waitForId")
            } else {
                Toast.init(text: "Task is not available. ", delay: Delay.short, duration: Delay.long).show()
            }
        } else {
            Toast.init(text: "Session is not initialized. ", delay: Delay.short, duration: Delay.long).show()
        }
 */
    }
    
    @IBAction func onFaceCaptureBtnClick() {
        if session != nil {
            let category = self.tasks.filtered(using: NSPredicate.init(format: "category like %@", CATEGORY_SELFIE))
            Swift.print(category)
            if category.count >= 1 {
                self.session?.resume(taskId: "waitForSelfieAware")
            } else {
                Toast.init(text: "Task is not available. ", delay: Delay.short, duration: Delay.long).show()
            }
        } else {
            Toast.init(text: "Session is not initialized. ", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    @IBAction func onLivenessButtonClick() {
       
        self.onAwareVerificationButtonClicked()
//        let captorLiveness : Captor? = tdiMobileSdk.captorProvider.get(name: CATEGORY_LIVENESS)
//        captorLiveness?.execute(callback: self)
        
    }
    
    func callAwareLiveNess() {
        if session != nil {
            let category = self.tasks.filtered(using: NSPredicate.init(format: "category like %@", CATEGORY_LIVENESS))
            Swift.print(category)
            if category.count >= 1 {
                self.session?.resume(taskId: "waitForSelfie")
            } else {
                Toast.init(text: "Task is not available. ", delay: Delay.short, duration: Delay.long).show()
            }
        } else {
            Toast.init(text: "Session is not initialized. ", delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    @IBAction func onResultButtonClick() {
        Swift.print("Result called")
        if session != nil {
            session?.result()
        } else {
            Toast(text: "Session is not initialized.",delay: Delay.short, duration: Delay.long).show()
        }
    }
    
    func showResultScreen(result : Result?) {
        let storyboardBundle = Bundle(for: TDIController.self)
        let storyBoard = UIStoryboard.init(name: "Main", bundle: storyboardBundle)
//        let resutlViewController = storyBoard.instantiateViewController(withIdentifier: "ResultVC")  as! ResultViewController
//        resutlViewController.resultInfo = result
//        self.navigationController?.pushViewController(resutlViewController, animated: true)
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .denied: alertPromptToAllowCameraAccessViaSetting()
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: break 
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
           // UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        if AVCaptureDevice.devices(for: AVMediaType.video).count > 0 {
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                DispatchQueue.main.async() {
                    self.checkCamera()
                }
            }
        }
    }    
}

extension TDIController : TdiMobileSdkDelegate {
   
    func taskStateListener(status : Status, task: NSMutableArray?, result: Result?) {
        
        self.tasks.removeAllObjects()
        self.tasks.addObjects(from: task as! [Any])
        Swift.print("task State listener is called /n Task : \(String(describing: task))")
        
        if isAwareTesting {
            self.callAwareLiveNess()
            isAwareTesting  = false
        }
        
        guard let resultValue = result else {
            return
        }
        
        Swift.print(resultValue)
        self.txtCode.text = result?.code
        self.txtMessage.text = result?.message
        
        if result?.description != nil {
            self.txtName.text = "Name = \(String(describing:(result?.description!["firstName"])))"
            self.txtDocVerif.text = "Document = \(String(describing:(result?.description!["docVerifyResultStatus"])))"
            self.txtFaceVerif.text = "Face = \(String(describing:(result?.description!["faceVerifResultStatus"])))"
            self.txtLivelinessVerif.text = "Liveliness = \(String(describing:(result?.description!["livelinessVerifResultStatus"])))"
        }
        self.showResultScreen(result: result)
    }
}

extension TDIController : CaptorCallback {
    func onDataCaptured(input: Input) {
        Toast(text: "Captured successfully.",delay: Delay.short, duration: Delay.long).show()
    }

    func onFailure(failure: Failure) {
        Toast(text: "Failed to capture.",delay: Delay.short, duration: Delay.long).show()
    }
}

extension TDIController : Captor {
    func execute(callback: CaptorCallback) {
        Swift.print("/n execute(callback: ) called ")
    }
}

extension TDIController : URLSessionDelegate {

    func callApi() {
        let session = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        let urlString = server.baseUrl + "/\(server.tennatID)/v1/scenarios"
        guard let url = URL.init(string: urlString) else {return}
        var request = URLRequest.init(url: url)
        
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Accept-Language": "en-US",
            "cache-control": "no-cache",
            "Authorization": server.token
        ]
        
        let parameters = ["name": "Spitfire-liveness-362ff854-0839-2453-14a7-958acb8650d1"] as [String : Any]
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])

        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
       let dataTask = session.dataTask(with: request) { (data, response, error) in
            Swift.print(response)
            if (error != nil) {
                Swift.print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                Swift.print(httpResponse)
            }
        }
        dataTask.resume()
    }
    
    static var rsa2048Asn1Header:[UInt8] = [
        0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86,
        0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00
    ]
    static let pinnedHash: [UInt8] = [0x98, 0xb4, 0xb2, 0xc8, 0xc3, 0xa1, 0xfa , 0x4f, 0x21, 0x40, 0xda, 0x41, 0xed, 0xa9, 0x0a, 0xae, 0xb1, 0x32, 0x5a, 0x81, 0xa7, 0x64, 0x71, 0xa0, 0xb5, 0xe6, 0xde, 0x26, 0x0f, 0x8f, 0x73, 0x6a]
    
    func sha256(_ data : Data) -> String {
        var keyWithHeader = Data(bytes: TDIController.rsa2048Asn1Header)
        keyWithHeader.append(data)
        var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
        keyWithHeader.withUnsafeBytes {
            _ = CC_SHA256($0, CC_LONG(data.count), &hash)
        }
        return Data(bytes: hash).base64EncodedString();
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(.useCredential, urlCredential)
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if(errSecSuccess == status) {
                    Swift.print(SecTrustGetCertificateCount(serverTrust))
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 1) {
                        
                        // Certificate pinning, uncomment to use this instead of public key pinning
                        //                        let serverCertificateData:NSData = SecCertificateCopyData(serverCertificate)
                        //                        let certHash = sha256(data: serverCertificateData as Data)
                        //                        if (certHash == pinnedCertificateHash) {
                        //                            // Success! This is our server
                        //                            completionHandler(.useCredential, URLCredential(trust:serverTrust))
                        //                            return
                        //                        }
                        
                        // Public key pinning
                        if #available(iOS 10.3, *) {
                            let serverPublicKey = SecCertificateCopyPublicKey(serverCertificate)
                            let serverPublicKeyData:NSData = SecKeyCopyExternalRepresentation(serverPublicKey!, nil )!
                            Swift.print(serverPublicKeyData as Data)
                            let keyHash = sha256(serverPublicKeyData as Data)
                            let keyBytes = Data(base64Encoded: keyHash)!
                            
                            if (keyBytes.elementsEqual(TDIController.pinnedHash)) {
                                // Success! This is our server
                                completionHandler(.useCredential, URLCredential(trust:serverTrust))
                                return
                            }
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(.cancelAuthenticationChallenge, nil)
    }
    
    
//    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
//            if let serverTrust = challenge.protectionSpace.serverTrust {
//                var secresult = SecTrustResultType.invalid
//                let status = SecTrustEvaluate(serverTrust, &secresult)
//
//                if (errSecSuccess == status) {
//                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
//                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
//                        let data = CFDataGetBytePtr(serverCertificateData)
//                        let size = CFDataGetLength(serverCertificateData)
//                        let cert1 = NSData(bytes: data, length: size)
//                        let file_der = Bundle.main.path(forResource: "StaginCertificate", ofType: "cer")
//
//                        if let file = file_der {
//                            if let cert2 = NSData(contentsOfFile: file) {
//                                if cert1.isEqual(to: cert2 as Data) {
//                                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
//                                    return
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        // Pinning failed
//        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
//    }
    
    /*
    (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    // get the public key offered by the server
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    SecKeyRef actualKey = SecTrustCopyPublicKey(serverTrust);
    
    // load the reference certificate
    NSString *certFile = [[NSBundle mainBundle] pathForResource:@"ref-cert" ofType:@"der"];
    NSData* certData = [NSData dataWithContentsOfFile:certFile];
    SecCertificateRef expectedCertificate = SecCertificateCreateWithData(NULL, (__bridge CFDataRef)certData);
    
    // extract the expected public key
    SecKeyRef expectedKey = NULL;
    SecCertificateRef certRefs[1] = { expectedCertificate };
    CFArrayRef certArray = CFArrayCreate(kCFAllocatorDefault, (void *) certRefs, 1, NULL);
    SecPolicyRef policy = SecPolicyCreateBasicX509();
    SecTrustRef expTrust = NULL;
    OSStatus status = SecTrustCreateWithCertificates(certArray, policy, &expTrust);
    if (status == errSecSuccess) {
    expectedKey = SecTrustCopyPublicKey(expTrust);
    }
    CFRelease(expTrust);
    CFRelease(policy);
    CFRelease(certArray);
    
    // check a match
    if (actualKey != NULL && expectedKey != NULL && [(__bridge id) actualKey isEqual:(__bridge id)expectedKey]) {
    // public keys match, continue with other checks
    [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
    } else {
    // public keys do not match
    [challenge.sender cancelAuthenticationChallenge:challenge];
    }
    if(actualKey) {
    CFRelease(actualKey);
    }
    if(expectedKey) {
    CFRelease(expectedKey);
    }
    }
 */
}

import Foundation
import SystemConfiguration

public class ConnectionCheck
{
    class func isConnectedToNetwork() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress,
                                                               {
                                                                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                                                                    SCNetworkReachabilityCreateWithAddress(nil, $0)
                                                                }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}

internal class Intent {
    var action = ""
    var hasExtra = ""
    var putExtra : Dictionary<AnyHashable,Any>!
    
    init(action : String) {
        self.action = action
    }
}
/*
class MyCaptor : Captor {
    init() {
    }
    
    var context : UIViewController!
    init(context : UIViewController) {
        self.context = context
    }
    
    /// It is used to execute the captor.
    ///
    /// - Parameter callback: CaptorCallBack protocol
    func execute(callback: CaptorCallback) {
        dPrint("Viewcontroller execute(callback: CaptorCallback)")
        let broadcastReceiver = TestResultReceiver.init(callback: callback)
        broadcastReceiver.callback = callback
        broadcastReceiver.registerForNotification(objName: "TestResultReceiver")
        self.getCaptorActivityIntent()
    }
    
    func getCaptorActivityIntent() -> UIViewController? {
        return self.context
    }
}

// swiftlint:enable all

/// This class is used to receive result from captor like passport and face detection.
class TestResultReceiver {
    var callback : CaptorCallback!
    
    init(callback : CaptorCallback) {
        self.callback = callback
    }
    
    /// Register for the notification to get notify after captor task finished.
    ///
    /// - Parameter objName: Name of notification
    func registerForNotification(objName : String) {
        dPrint("Viewcontroller registerForNotification")
        let name = NSNotification.Name.init(rawValue: objName)
        NotificationCenter.default.addObserver(forName: name, object: nil, queue: nil) { (notification) in
            self.onReceive(info: notification as NSNotification)
        }
    }
    
    /// Receive the notification after tasks get finished
    ///
    /// - Parameter info: NSNotification object
    @objc func onReceive(info: NSNotification) {
        dPrint("View controller :(info: NSNotification)")
        let name = NSNotification.Name.init(rawValue: ACTION_CAPTURE_DATA)
        NotificationCenter.default.removeObserver(self, name: name, object: nil)
        guard let intent = info.userInfo?["intent"] as? Intent else {
            return
        }
        if ACTION_CAPTURE_DATA == intent.action {
            if intent.putExtra.keys.contains(EXTRA_IMAGE_DATA) {
                guard let data = intent.putExtra[EXTRA_IMAGE_DATA] as? InputImpl else {
                    return
                }
                callback.onDataCaptured(input: data)
            } else {
                var failure : Failure!
                if intent.putExtra.keys.contains(EXTRA_FAILURE_DATA) {
                    guard let data = intent.putExtra[EXTRA_FAILURE_DATA] as? Failure else {
                        return
                    }
                    failure = data
                } else {
                    dPrint("Failed")
//                    FailureImpl.init(errorCode: -1, errorMessage: "Unknown error")
                }
                callback.onFailure(failure: failure)
            }
        }
    }
}
*/
